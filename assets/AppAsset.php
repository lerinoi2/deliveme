<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'https://fonts.googleapis.com/icon?family=Material+Icons',
        'css/libs/fonts.css',
        'css/libs/materialize.min.css',
        'css/libs/datetimepicker.css',
        'css/libs/morris.css',
        'css/main.css',
    ];
    public $js = [
        'js/libs/materialize.min.js',
        'js/libs/datetimepicker.js',
        'js/libs/raphael.min.js',
        'js/libs/morris.min.js',
        'js/libs/socket.io.js',
        'js/libs/onesignal.js',
        'js/general.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
