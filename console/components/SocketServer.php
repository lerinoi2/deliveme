<?php

namespace app\console\components;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class SocketServer implements MessageComponentInterface
{
    protected $clients;
    public function __construct()
    {
        $this->clients = array(); // Для хранения технической информации об присоединившихся клиентах используется технология SplObjectStorage, встроенная в PHP
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients[$conn->resourceId] = array('conn' => $conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $data = json_decode($msg, true);
        switch($data['action']){
            case 'setID':
                $this->clients[$from->resourceId]['ID'] = $data['data'];
                break;
            default:
                $this->sendMessages($data);
                break;
        }

        echo $msg;
    }

    public function sendMessages($arMessages)
    {
        foreach ($arMessages as $arMess){
            foreach ($this->clients as $channel){
                if($channel['ID'] === $arMess['channel_id']){
                    $channel['conn']->send(json_encode($arMess['data']));
                }
            }
        }

    }

    public function onClose(ConnectionInterface $conn)
    {
        unset($this->clients[$conn->resourceId]);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}