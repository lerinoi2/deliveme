<?php

namespace app\console\controllers;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use app\console\components\SocketServer; //не забудьте поменять, если отличается

class SocketController extends  \yii\console\Controller
{
    public static function actionStartSocket($port = 8787)
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new SocketServer()
                )
            ),
            $port
        );
        $server->run();
    }
}