<?php

namespace app\modules\webhooks\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bot\models\UserBot;
use app\modules\webhooks\models\VKApi;
use app\modules\webhooks\models\VKMessage;

/**
 * Default controller for the `dialog` module
 */
class VkController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionSave()
    {
        $data = json_decode(Yii::$app->request->post('message'), true);

        //return json_encode($data);

        $res = array();

        foreach ($data as $message){
            $tgMessage = new VKMessage(false, $message);
            //array_push($res, $tgMessage->user->id);
            array_push($res, $tgMessage->save());
        }

        return json_encode($res);
    }

    public function actionSend($user_id)
    {
        $text = Yii::$app->request->post('text');

        $vk = new VKApi($user_id);
        $vk->sendMessage($text);

        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionUser($user_id)
    {
        $vk = new VKApi($user_id);
        return $vk->getUserInfo();
    }

    public function actionDisable($user_id)
    {
        $vk = new VKApi($user_id);
        $disable = json_decode($vk->disableChat(), true);

        if($disable['ok']){
            $user = UserBot::findOne($user_id);
            $user->connect = 2;
            if ($user->save(false)) {
                return true;
            }
        }else{
            return json_encode(array('error' => 'Не удалось заблокировать чат с ботом'));
        }
    }

    public function actionEnable($user_id)
    {
        $vk = new VKApi($user_id);
        $enable = json_decode($vk->enableChat(), true);

        if($enable['ok']){

            $user = UserBot::findOne($user_id);
            $user->connect = 0;

            if ($user->save(false)) {
                return true;
            }
        }else{
            return json_encode(array('error' => 'Не удалось разблокировать чат'));
        }
    }
}
