<?php

namespace app\modules\webhooks\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bot\models\UserBot;
use app\modules\review\models\Review;
use app\modules\bot\models\Bot;
use app\modules\bot\models\Platform;
use app\modules\webhooks\models\TelegramApi;
use app\modules\webhooks\models\TelegramMessage;

/**
 * Default controller for the `dialog` module
 */
class TelegramController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionSave()
    {
        $data = json_decode(Yii::$app->request->post('message'), true);

        $res = array();

        foreach ($data as $message){
            $tgMessage = new TelegramMessage(false, $message);
            array_push($res, $tgMessage->save());
        }

        return json_encode($res);
    }

    public function actionSend($user_id)
    {
        $text = Yii::$app->request->post('text');

        $tgConnect = new TelegramApi($user_id);
        $tgConnect->sendMessage($text);

        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionDisable($user_id)
    {
        UserBot::updateAll(['connect' => 2], ['id' => $user_id]);

        $tgConnect = new TelegramApi($user_id);
        $tgConnect->disableChat($_GET['review_id']);

        // UserBot::updateAll(['connect' => 2], ['id' => $user_id]);
        // $user = UserBot::findOne($user_id);
        // $user->connect = 2;
        // if ($user->save(false)) {
        //     return true;
        // }
        return true;
        // return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionEnable($user_id)
    {
        UserBot::updateAll(['connect' => 0], ['id' => $user_id]);

        $tgConnect = new TelegramApi($user_id);
        $tgConnect->enableChat();

        // $user = UserBot::findOne($user_id);
        // $user->connect = 0;

        // if ($user->save(false)) {
        //     return true;
        // }
        return true;
        // return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionTest(){
        $reviews = Review::find()->where(['not', ['mark' => null]])->all();
        $res = '';

        foreach ($reviews as $review){
            $res .= $review->sendReviewAlert();
        }

        return count($reviews);;
    }

    public function actionUpdate($user_id)
    {
        $connect = new TelegramApi($user_id);
        $connect->user->image = $connect->saveUserImage();
        $connect->user->save(false);

        //return $connect->user->image;
        return $this->redirect(Yii::$app->request->referrer);
    }
}
