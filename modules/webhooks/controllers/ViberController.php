<?php

namespace app\modules\webhooks\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bot\models\UserBot;
use app\modules\bot\models\Bot;
use app\modules\bot\models\Platform;
use app\modules\webhooks\models\ViberApi;
use app\modules\webhooks\models\ViberMessage;

/**
 * Default controller for the `dialog` module
 */
class ViberController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionSave()
    {
        $data = json_decode(Yii::$app->request->post('message'), true);
        $res = array();

        foreach ($data as $message){
            $tgMessage = new ViberMessage(false, $message);
            array_push($res, $tgMessage->save());
        }

        return json_encode($res);
    }

    public function actionSend($user_id)
    {
        $text = Yii::$app->request->post('text');

        $viberConnect = new ViberApi($user_id);
        $viberConnect->sendMessage($text);

        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionDisable($user_id)
    {
        UserBot::updateAll(['connect' => 2], ['id' => $user_id]);

        $viberConnect = new ViberApi($user_id);
        $viberConnect->disableChat();

        // $user = UserBot::findOne($user_id);
        // $user->connect = 2;
        // // $user->connect = time();
        // // $user->check = time();
        // if ($user->save(false)) {
        //     return true;
        // }
        return true;
        // return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionEnable($user_id)
    {
        UserBot::updateAll(['connect' => 0], ['id' => $user_id]);

        $viberConnect = new ViberApi($user_id);
        $viberConnect->enableChat();

        // $user = UserBot::findOne($user_id);
        // $user->connect = 0;
        // if ($user->save(false)) {
        //     return true;
        // }
        return true;
        // return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionUpdate($user_id)
    {
        $connect = new ViberApi($user_id);
        $connect->user->image = $connect->saveUserImage();
        $connect->user->save(false);

        //return $connect->user->image;
        return $this->redirect(Yii::$app->request->referrer);
    }
}
