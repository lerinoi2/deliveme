<?php

namespace app\modules\webhooks\models;

use Yii;
use yii\helpers\FileHelper;

class Files{
    public static function saveFromUrl($url, $dir = false, $name = ''){
        $path = explode('?', $url);
        $ext = end(explode('.', $path[0]));
        $fileName = $name.'.'.$ext;
        $path = '/web/uploads/'.($dir ? $dir.'/' : '');
        $rootPath = $_SERVER['DOCUMENT_ROOT'].$path;
        FileHelper::createDirectory($rootPath);
        $fullPath = $rootPath.$fileName;
        $path = $path.$fileName;
        $fp = fopen ($fullPath, 'w+');

        $options = [
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FILE => $fp,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_TIMEOUT => 1000,
            CURLOPT_USERAGENT => 'any',
            CURLOPT_VERBOSE => true,
            CURLOPT_PROXY => "18.188.16.18:1080",
            CURLOPT_PROXYTYPE => 7,
            CURLOPT_PROXYUSERPWD => "ckdigital:xrPkjtAQDU*n@5bn",
        ];
        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);
        fwrite($fp, $res);
        fclose($fp);

        $error = curl_error($curl);

        return $error ? $error : $path;
    }
}