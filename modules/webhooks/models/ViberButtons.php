<?php

namespace app\modules\webhooks\models;

use Yii;
use app\modules\bot\models\Message;

/**
 * This is the model class for table "viber_buttons".
 *
 * @property int $id
 * @property int $columns
 * @property int $rows
 * @property string $bg_color
 * @property int $silent
 * @property string $bg_media_type
 * @property string $bg_media
 * @property int $bg_loop
 * @property string $action_type
 * @property string $action_body
 * @property string $image
 * @property string $text
 * @property string $text_v_align
 * @property string $text_h_align
 * @property string $text_paddings
 * @property int $text_opacity
 * @property string $text_size
 * @property string $open_u_r_l_type
 * @property string $open_u_r_l_media_type
 * @property string $text_bg_gradient_color
 * @property int $message_id
 *
 * @property Messages $message
 */
class ViberButtons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'viber_buttons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['columns', 'rows', 'text_opacity', 'message_id'], 'integer'],
            [['message_id'], 'required'],
            [['bg_color', 'bg_media_type', 'bg_media', 'action_type', 'action_body', 'image', 'text', 'text_v_align', 'text_h_align', 'text_paddings', 'text_size', 'open_u_r_l_type', 'open_u_r_l_media_type', 'text_bg_gradient_color'], 'string', 'max' => 255],
            [['silent', 'bg_loop'], 'string', 'max' => 1],
            [['message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::className(), 'targetAttribute' => ['message_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'columns' => 'Columns',
            'rows' => 'Rows',
            'bg_color' => 'Bg Color',
            'silent' => 'Silent',
            'bg_media_type' => 'Bg Media Type',
            'bg_media' => 'Bg Media',
            'bg_loop' => 'Bg Loop',
            'action_type' => 'Action Type',
            'action_body' => 'Action Body',
            'image' => 'Image',
            'text' => 'Text',
            'text_v_align' => 'Text V Align',
            'text_h_align' => 'Text H Align',
            'text_paddings' => 'Text Paddings',
            'text_opacity' => 'Text Opacity',
            'text_size' => 'Text Size',
            'open_u_r_l_type' => 'Open URL Type',
            'open_u_r_l_media_type' => 'Open URL Media Type',
            'text_bg_gradient_color' => 'Text Bg Gradient Color',
            'message_id' => 'Message ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Messages::className(), ['id' => 'message_id']);
    }

    public static function saveFromArray($arBtn, $message_id)
    {
        $button = new self();
        $arkeys = array();
        foreach ($arBtn as $key => $value)
        {
            $attr = array();
            preg_match_all('/[A-Z][^A-Z]*?/Usu', $key, $attr);
            $attr = $attr[0];
            foreach ($attr as $k => $word){
                $attr[$k] = strtolower($word);
            }
            $attrName = implode('_', $attr);

            array_push($arkeys, $attrName);

            $button->{$attrName} = is_array($value) ? json_encode($value) : $value;
        }
        //$button->text = json_encode($arkeys);
        $button->message_id = $message_id;
        $button->save(false);

        return $button;
    }
}
