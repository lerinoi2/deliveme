<?php

namespace app\modules\webhooks\models;

use app\modules\bot\models\Platform;
use app\modules\bot\models\UserBot;

class ViberApi{
    public $integration;
    public $user;
    public $data;

    public function __construct($user_id = null, $postData = null) {
        if($user_id){
            $this->user = UserBot::find()
                ->where(['id' => $user_id, 'platform' => 1])
                ->one();

            $this->integration = Platform::find()
                ->where(['bot_id' => $this->user->bot_id, 'platform_id' => 2])
                ->one();
        }

        if($postData){
            $sender = $postData['request']['sender'];
            if(!$this->integration){
                $this->integration = Platform::find()->where(['token' => $postData['additions']['apiKey']])->one();
            }


            if(!$this->user){
                $this->user = UserBot::getUser($sender, $this->integration->bot_id, 1); // sender, bot_id, platform
            }

        }

        $this->data = $postData;

        $this->mail = array(
            'response' => false,
            'apiResponse' => false
        );
    }

    public function disableChat($review_id = false){

        $action = array(
            'type' => 'disable',
            'chat_id' => $this->user->chat_id
        );

        if($review_id){
            $this->user->active_review_id = intval($review_id);
            $this->user->save();
        }

        $this->sendButton(array(
            'ActionType' => 'none',
            'ActionBody' => 'none',
            'BgColor' => '#0a78d6',
            'Columns' => 6,
            'Rows' => 2,
            'Text' => "<font color='#FFFFFF'>К чату подключился оператор</font>",
            'TextSize' => 'large'
        ));

        return $this->callBot($action);
    }

    public function enableChat(){
        $action = array(
            'type' => 'enable',
            'chat_id' => $this->user->chat_id
        );
        $this->user->active_review_id = 0;
        $this->user->save();
        $this->sendButton(array(
                'ActionBody' => 'none',
                'ActionType' => 'none',
                'BgColor' => '#0a78d6',
                'Columns' => 6,
                'Rows' => 2,
                'Text' => "<font color='#FFFFFF'>Оператор отключился от чата</font>",
                'TextHAlign' => 'center',
                'TextSize' => 'large'
            )
        );
        // $this->sendButton(array(
        //         'ActionBody' => 'Старт',
        //         'ActionType' => 'reply',
        //         'BgColor' => '#603ccc',
        //         'Columns' => 6,
        //         'Rows' => 1,
        //         'Text' => "<font color='#FFFFFF'>Вернуться к боту</font>",
        //         'TextHAlign' => 'center',
        //         'TextSize' => 'large'
        //     )
        // );

        return $this->callBot($action);
    }

    public function callBot($data){
        $options = [
            CURLOPT_URL => $this->integration->bot_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
        ];


        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);

        return $res;
    }

    public function sendButton($arButton)
    {
        $rows = $arButton['Rows'];
        $data = [
            'receiver' => $this->user->chat_id,
            'min_api_version' => 2,
            'type' => 'rich_media',
            'sender' => array(
                'name' => 'Оператор',
                // 'avatar' => 'https://s3.amazonaws.com/profile_photos/140293045442935.bjgX2ZfHoZ5vDvahST1X_128x128.png'
            ),
            'rich_media' => array(
                'BgColor' => '#FFFFFF',
                'Type' => 'rich_media',
                'ButtonsGroupCollumns' => 6,
                'ButtonsGroupRows' => $rows,
                'Buttons' => array($arButton)
            )
        ];

        $apiResponse = $this->call('send_message', $data);

        $result = array(
            'response' => array(
                'data' => $data,
                'method' => 'send_message'
            ),
            'apiResponse' => $apiResponse
        );

        $tgMessage = new ViberMessage($this->user->id, $result);
        return $tgMessage->save();
    }

    public function sendKeyboard($keyboard, $text)
    {
        $data = [
            'receiver' => $this->user->chat_id,
            'min_api_version' => 1,
            'type' => 'text',
            'text' => $text,
            'sender' => array(
                'name' => 'Оператор',
                // 'avatar' => 'https://s3.amazonaws.com/profile_photos/140293045442935.bjgX2ZfHoZ5vDvahST1X_128x128.png'
            ),
            'keyboard' => $keyboard
        ];

        $apiResponse = $this->call('send_message', $data);

        $result = array(
            'response' => array(
                'data' => $data,
                'method' => 'send_message'
            ),
            'apiResponse' => $apiResponse
        );

        $tgMessage = new ViberMessage($this->user->id, $result);
        return $tgMessage->save();
    }

    public function sendMessage($text, $keyboard = false){
        $data = [
            'receiver' => $this->user->chat_id,
            'text' => $text,
            'type' => 'text',
            /*'sender' => array(
                'name' => 'Оператор',
                'avatar' => 'https://s3.amazonaws.com/profile_photos/140293045442935.bjgX2ZfHoZ5vDvahST1X_128x128.png'
            )*/
        ];

        $apiResponse = $this->call('send_message', $data);

        $result = array(
            'response' => array(
                'data' => $data,
                'method' => 'send_message'
            ),
            'apiResponse' => $apiResponse
        );

        $tgMessage = new ViberMessage($this->user->id, $result);
        return $tgMessage->save();
    }

    public function getUserProfile(){
        return $this->call('get_user_details', array('id' => $this->user->chat_id))['user'];
    }
    private function call($method, $data){

        $options = [
            CURLOPT_URL => 'https://chatapi.viber.com/pa/'.$method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array('X-Viber-Auth-Token: '.$this->integration->token)
        ];


        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);

        return $res ? json_decode($res, true)  : array('status' => 500, 'status_message' => curl_error($curl));
    }

    public function saveUserImage()
    {
        return $this->getUserProfile()['avatar'];
    }
}