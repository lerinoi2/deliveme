<?php

namespace app\modules\webhooks\models\api;

use app\modules\webhooks\models\VKMessage;

class Vk{
    const API_VERSION = '5.74';
    const API_URL = 'https://api.vk.com/method/';
    private $TOKEN;
    private $user_id;
    public function __construct($token, $user_id = false)
    {
        $this->TOKEN = $token;
        $this->user_id = $user_id;
    }
    public function sendMessage($user_id, $message){
        return $this->call('messages.send', array(
            'peer_id'    => $user_id,
            'message'    => $message
        ));
    }
    public function getUser($user_id){
        return $this->call('users.get', array(
            'user_ids' => $user_id,
            'fields' => array('photo_100')
        ));
    }
    private function call($method, $data)
    {
        $query = http_build_query(array_merge($data, array(
            'access_token' => $this->TOKEN,
            'v' => self::API_VERSION
        )));
        $url = self::API_URL.$method.'?'.$query;
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        ];


        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);

        $apiResponse = curl_error($curl) ? curl_error($curl) : json_decode($res, true)['response'];

        $result = array(
            'additions' => array(
                'token' => $this->TOKEN
            ),
            'response' => array(
                'data' => $data,
                'method' => $method
            ),
            'apiResponse' => array('response' => $apiResponse)
        );

        $VKm = new VKMessage($this->user_id, $result);
        $VKm->save();

        return $apiResponse;
    }
}