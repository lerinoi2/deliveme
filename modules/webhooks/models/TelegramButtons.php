<?php

namespace app\modules\webhooks\models;

use Yii;
use app\modules\webhooks\models\TelegramMessage;

/**
 * This is the model class for table "telegram_buttons".
 *
 * @property int $id
 * @property string $callback_json
 * @property string $text
 */
class TelegramButtons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_buttons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['callback_json', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'callback_json' => 'Callback Json',
            'text' => 'Text',
        ];
    }

    public static function saveKeyboard($arButtons, $message_id)
    {
        foreach ($arButtons as $arButton){
            $btn = self::getButton($arButton);
            self::saveLink($btn->id, $message_id);
        }
    }

    private static function saveLink($btn_id, $message_id)
    {
        $link = new TelegramMessageButtons();
        $link->button_id = $btn_id;
        $link->message_id = $message_id;
        $link->save();

        return $link->id;
    }

    public static function getButton($arButton)
    {
        $btn = self::findOne(['callback_json' => json_encode($arButton['callback_data'])]);

        if(!$btn){
            $btn = self::createButton($arButton);
        }

        return $btn;
    }
    public static function getViberButton($arButton)
    {
        $btn = self::findOne(['callback_json' => json_encode($arButton['ActionBody'])]);

        if(!$btn){
            $btn = self::createViberButton($arButton);
        }

        return $btn;
    }

    public static function createViberButton($arButton)
    {
        $btn = new self();
        $btn->callback_json = json_encode($arButton['ActionBody']);
        $btn->text = TelegramMessage::removeEmoji($arButton['text']);
        $btn->save();

        return $btn;
    }
    public static function createButton($arButton)
    {
        $btn = new self();
        $btn->callback_json = json_encode($arButton['callback_data']);
        $btn->text = TelegramMessage::removeEmoji($arButton['text']);
        // $btn->text = 'test';
        $btn->save();

        return $btn;
    }


    public static function saveViberKeyboard($arButtons, $message_id)
    {
        foreach ($arButtons as $arButton){
            $btn = self::getViberButton($arButton);
            self::saveLink($btn->id, $message_id);
        }
    }
}
