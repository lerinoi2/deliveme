<?php

namespace app\modules\webhooks\models;

use Yii;

/**
 * This is the model class for table "telegram_message_buttons".
 *
 * @property int $id
 * @property int $message_id
 * @property int $button_id
 */
class TelegramMessageButtons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_message_buttons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'button_id'], 'required'],
            [['message_id', 'button_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message_id' => 'Message ID',
            'button_id' => 'Button ID',
        ];
    }
}
