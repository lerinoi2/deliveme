<?php

namespace app\modules\webhooks\models;

use app\modules\bot\models\Platform;
use app\modules\bot\models\UserBot;
use app\modules\webhooks\models\api\Vk;

class VKApi{
    public $integration;
    public $user;
    public $data;

    public function __construct($user_id = null, $postData = null) {
        if($user_id){
            $this->user = UserBot::find()
                ->where(['id' => $user_id, 'platform' => 2])
                ->one();

            $this->integration = Platform::find()
                ->where(['bot_id' => $this->user->bot_id, 'platform_id' => 3])
                ->one();
        }

        if($postData){
            if(!$this->integration){
                $this->integration = Platform::find()->where(['token' => $postData['additions']['token']])->one();
            }

            if(!$this->user){
                $this->user = UserBot::getUser(array('id' => $postData['request']['object']['user_id']), $this->integration->bot_id, 2);
                if($this->user->name == $postData['request']['object']['user_id']){
                    $user = $this->getUserInfo();
                    $this->user->name = $user['first_name'].' '.$user['last_name'];
                    $this->user->image = $user['photo_100'];
                    $this->user->save();
                }
            }
        }

        $this->data = $postData;

        $this->mail = array(
            'response' => false,
            'apiResponse' => false
        );
    }

    public function getUserInfo(){
        return $this->VK()->getUser($this->user->chat_id)[0];
    }

    public function VK(){
        return new Vk($this->integration->token, $this->user->id);
    }

    public function disableChat(){

        $action = array(
            'type' => 'disable',
            'chat_id' => $this->user->chat_id
        );
        return $this->callBot($action);
    }

    public function enableChat(){
        $action = array(
            'type' => 'enable',
            'chat_id' => $this->user->chat_id
        );
        return $this->callBot($action);
    }

    public function callBot($data){
        $options = [
            CURLOPT_URL => $this->integration->bot_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
        ];


        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);

        return $res;
    }

    public function sendMessage($text = ''){
        return $this->VK()->sendMessage($this->user->chat_id, $text);
    }

    public function getUserProfile(){
        return $this->call('get_user_details', array('id' => $this->user->chat_id))['user'];
    }
    private function call($method, $data){

        $options = [
            CURLOPT_URL => 'https://chatapi.viber.com/pa/'.$method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array('X-Viber-Auth-Token: '.$this->integration->token)
        ];


        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);

        return $res ? json_decode($res, true)  : array('status' => 500, 'status_message' => curl_error($curl));
    }
}