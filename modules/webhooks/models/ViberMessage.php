<?php

namespace app\modules\webhooks\models;

use app\modules\bot\models\Message;
use app\modules\review\models\Review;

class ViberMessage extends ViberApi{
    private $review;
    public function save(){

        if($this->data['additions']['review']){
            $this->review = Review::getByUserID($this->user->id);
            if($this->data['additions']['stars']){
                $this->review->mark = $this->data['additions']['stars'];
                $this->review->save();
            }
        }

        if($this->data['additions']['phone']){
            $this->user->savePhoneNumber($this->data['additions']['phone']);
        }

        if($this->user->active_review_id){
            $this->review = Review::findOne(['id' => $this->user->active_review_id]);
        }

        if($this->data['additions']['close']){
            $this->user->closeChat();
        }

        $result = array(
            'request' => $this->saveRequest(),
            'response' => $this->saveResponse()
        );

        if($this->data['additions']['review_last_message'] && $this->review){
            $this->review->complete();
        }

        return json_encode($result);
    }
    private function saveRequest(){
        if($this->isRequestExist()) return false;

        $request = $this->data['request'];

        if($request['message']){

            switch ($request['message']['type']){
                case 'text':
                    $isCallback = is_array($request['message']['text']);
                    $result = array(
                        'text' => $isCallback ? json_encode($request['message']['text']) : $request['message']['text'],
                        'message_id' => $request['message_token'],
                        'date' => date('Y-m-d H:i:s'),
                        'action' => $isCallback ? 'pressButton' : 'message',
                        'is_bot' => 0,
                        'user_bot_id' => $this->user->id
                    );
                    break;
                case 'picture':
                    $result = array(
                        'text' => Files::saveFromUrl($request['message']['media'], 'viber/pictures', $request['message_token']),
                        'message_id' => $request['message_token'],
                        'date' => date('Y-m-d H:i:s'),
                        'action' => 'sendSticker',
                        'is_bot' => 0,
                        'user_bot_id' => $this->user->id
                    );
                    break;
                case 'sticker':
                    $result = array(
                        'text' => Files::saveFromUrl($request['message']['media'], 'viber/pictures', $request['message_token']),
                        'message_id' => $request['message_token'],
                        'date' => date('Y-m-d H:i:s'),
                        'action' => 'sendSticker',
                        'is_bot' => 0,
                        'user_bot_id' => $this->user->id
                    );
                    break;
                case 'contact':
                    if(!$request['message']['contact']['phone_number']) return false;
                    $result = array(
                        'text' => $request['message']['contact']['phone_number'],
                        'message_id' => $request['message_token'],
                        'date' => date('Y-m-d H:i:s'),
                        'action' => 'message',
                        'is_bot' => 0,
                        'user_bot_id' => $this->user->id
                    );
                    $this->user->savePhoneNumber($request['message']['contact']['phone_number']);
                    break;
                default:
                    $result = array(
                        'text' => json_encode($request['message']),
                        'message_id' => $request['message_token'],
                        'date' => date('Y-m-d H:i:s'),
                        'action' => 'unknown',
                        'is_bot' => 0,
                        'user_bot_id' => $this->user->id
                    );
                    break;
            }
        }else{
            return false;
        }
        if($this->review){
            $result['review_id'] = $this->review->id;
        }
        if($this->data['additions']['call']){
            $result['action'] = 'connect';
        }

        if($this->data['additions']['order']){
            $this->user->addOrder();
        }
        if(!$result['text']) $result['text'] = '';
        return $this->createMessage($result);
    }
    private function saveResponse(){

        $response = $this->data['response'];
        if(!$response) return false;

        $apiResponse = $this->data['apiResponse'];
        if($apiResponse['status'] === 0) {
            switch ($response['data']['type']) {
                case 'text':
                    $result = array(
                        'text' => $response['data']['text'],
                        'message_id' => $apiResponse['result']['message_token'],
                        'action' => 'sendMessage',
                        'date' => date('Y-m-d H:i:s'),
                        'is_bot' => 1,
                        'user_bot_id' => $this->user->id
                    );
                    break;
                case 'rich_media':
                    $result = array(
                        'text' => '',
                        'message_id' => $apiResponse['result']['message_token'],
                        'action' => 'sendMessage',
                        'date' => date('Y-m-d H:i:s'),
                        'is_bot' => 1,
                        'user_bot_id' => $this->user->id
                    );
                    break;
                case 'picture':
                    $result = array(
                        'text' => $response['data']['media'],
                        'message_id' => $apiResponse['result']['message_token'],
                        'action' => 'sendSticker',
                        'date' => date('Y-m-d H:i:s'),
                        'is_bot' => 1,
                        'user_bot_id' => $this->user->id
                    );
                    break;
                default:
                    if($response['data']['keyboard']){
                        return false;
                    }
                    $result = array(
                        'text' => json_encode($response['data']),
                        'message_id' => $apiResponse['result']['message_token'],
                        'action' => 'unknown',
                        'date' => date('Y-m-d H:i:s'),
                        'is_bot' => 1,
                        'user_bot_id' => $this->user->id
                    );
                    break;
            }
        }else{
            $result = array(
                'text' => $apiResponse['status_message'],
                'message_id' => $apiResponse['message_token'],
                'date' => date('Y-m-d H:i:s'),
                'action' => 'apiError',
                'is_bot' => 1,
                'user_bot_id' => $this->user->id
            );
        }

        if($this->review){
            $result['review_id'] = $this->review->id;
        }
        if(!$result['text']) $result['text'] = '';
        $message_id = $this->createMessage($result);

        if($response['data']['rich_media'] && is_array($response['data']['rich_media']['Buttons'])){
            foreach($response['data']['rich_media']['Buttons'] as $btn){
                ViberButtons::saveFromArray($btn, $message_id);
            }
        }

        return $message_id;
    }
    private function isRequestExist(){
        $message_id = $this->data['request']['message_token'];


        $message = Message::findOne(['message_id' => $message_id]);
        if($message){
            return true;
        }

        return false;
    }
    private function createMessage($arFields)
    {
        $msg = new Message();
        $msg->attributes = $arFields;
        $msg->save(false);

        return $msg->id;
    }
}