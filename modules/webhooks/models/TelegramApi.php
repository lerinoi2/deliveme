<?php

namespace app\modules\webhooks\models;

use app\modules\bot\models\Platform;
use app\modules\bot\models\UserBot;

class TelegramApi{
    const BASE_URL = 'https://api.telegram.org/bot';

    protected $integration;
    protected $data;
    public $user;

    public function __construct($user_id = null, $postData = null) {
        if($user_id){
            $this->user = UserBot::find()
                ->where(['id' => $user_id])
                ->one();

            $this->integration = Platform::find()
                ->where(['bot_id' => $this->user->bot_id, 'platform_id' => 1])
                ->one();
        }

        if($postData){
            $msg = $postData['request']['callback_query'] ? $postData['request']['callback_query']['message'] : $postData['request']['message'];

            if(!$this->user){
                $this->integration = Platform::findOne(['token' => $postData['additions']['token']]);
                $this->user = UserBot::getUser($msg['chat'], $this->integration->bot_id, 1);
            }

        }

        $this->data = $postData;

        $this->mail = array(
            'response' => false,
            'apiResponse' => false
        );
    }

    public function getBotData($postData){
        if($postData['apiResponse']['result']['from']){
            return $postData['apiResponse']['result']['from'];
        }else if($postData['request']['callback_query']['message']['from']['is_bot']){
            return $postData['request']['callback_query']['message']['from'];
        }else{
            return false;
        }
    }

    public function disableChat($review_id){

        $action = array(
            'type' => 'disable',
            'chat_id' => $this->user->chat_id
        );

        if($review_id){
            $this->user->active_review_id = intval($review_id);
            $this->user->save();
        }

        // $this->sendMessage('<b>К чату подключился оператор</b>', json_encode(array(
        //     'remove_keyboard' => true
        // )));
        $this->sendMessage('<b>К чату подключился оператор</b>', json_encode([
            'inline_keyboard' => [[[
                'text' => 'Отключиться от чата',
                'callback_data' => '{"type": "back"}'
            ]]]
        ]));

        return $this->callBot($action);
    }

    public function enableChat(){
        $action = array(
            'type' => 'enable',
            'chat_id' => $this->user->chat_id
        );

        // $this->sendMessage('<b>Оператор отключился от чата</b>', json_encode(
        //     array( 
        //         'keyboard' => [['Назад']],
        //         'resize_keyboard' => true,
        //         'one_time_keyboard' => true
        //     )
        // ));

        $this->user->active_review_id = 0;
        $this->user->save();
        return $this->callBot($action);
    }

    public function getDefaultKeyboard(){
        if($this->integration->default_keyboard_json){
            return json_decode($this->integration->default_keyboard_json, true);
        }else{
            $config = $this->getBotConfigArray();
            if($config['keyboard']){
                $this->integration->default_keyboard_json = json_encode($config['keyboard']);
                $this->integration->save();
                return $config['keyboard'];
            }else{
                return array(array(array('text' => '/start')));
            }
        }
    }

    public function getBotConfigArray(){
        $response = json_decode($this->callBot(array('type' => 'get_config')), true);
        if($response['ok']){
            return $response['config'];
        }else{
            return false;
        }
    }

    public function callBot($data){
        $options = [
            CURLOPT_URL => $this->integration->bot_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
        ];


        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);

        return $res;
    }

    public function sendMessage($text, $keyboard = false){
        $data = [
            'chat_id' => $this->user->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML',
        ];

        if($keyboard){
            $data['reply_markup'] = $keyboard;
        }

        $result = array(
            'response' => array(
                'data' => $data,
                'method' => 'manager'
            ),
            'apiResponse' => $this->call('sendMessage', $data)
        );

        $tgMessage = new TelegramMessage($this->user->id, $result);
        return $tgMessage->save();
    }

    public function sendPhoto($photo) {
        $data = [
            'chat_id' => $this->user->chat_id,
            'photo' => $photo,
        ];

        return $this->call('sendPhoto', $data);
    }

    public function saveUserImage(){
        $tgResp = $this->getUserPhoto($this->user->chat_id);
        if($tgResp['ok']){
            $file_path = $tgResp['result']['photos'][0][0]['file_path'];
            $file_id = $tgResp['result']['photos'][0][0]['file_id'];

            if(!$file_path){
                $getFile = $this->getFile($file_id);
                if($getFile['ok']){
                    $file_path = $getFile['result']['file_path'];
                }else{
                    return null;
                }
            }

            return Files::saveFromUrl('https://api.telegram.org/file/bot'.$this->integration->token.'/'.$file_path, 'users', 'user_'.$this->user->chat_id);
        }else{
            return null;
        }
    }
    public function getUserPhoto($user_id){
        return $this->call(
            'getUserProfilePhotos',
            array(
                'user_id' => $user_id,
                'offset' => 0,
                'limit' => 1
            )
        );
    }
    public function getFile($file_id){
        return $this->call(
            'getFile',
            array(
                'file_id' => $file_id
            )
        );
    }
    private function call($method, $data){

        $options = [
            CURLOPT_URL => self::BASE_URL.$this->integration->token.'/'.$method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_PROXY => "18.188.16.18:1080",
            CURLOPT_PROXYTYPE => 7,
            CURLOPT_PROXYUSERPWD => "ckdigital:xrPkjtAQDU*n@5bn",
        ];


        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $res = curl_exec($curl);

        if($res){
            return json_decode($res, true);
        }else{
            return array('ok' => false, 'description' => 'CURL Error: '.curl_error($curl));
        }
    }
}