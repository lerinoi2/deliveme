<?php

namespace app\modules\bot\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bot\models\Message;
use app\modules\bot\models\UserBot;
use app\modules\bot\models\Bot;

/**
 * Default controller for the `bot` module
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionMessage($id)
    {
        $user_id = Yii::$app->request->get('user_id');
        $user = UserBot::findOne($user_id);
        $message = $user->getMessagesWithButtons([Message::tableName().'.id' => (int)$id]);

        //return json_encode($message);
        return $this->renderPartial('_message', [
            'message' => $message[$id],
            'platform' => $user->platformName
        ]);
    }

    public function actionGetuserinfo()
    {
        $bot = Bot::find()->where(['user_id' => Yii::$app->user->id])->one();

        return json_encode([
            'messages' => $bot->unreadMessages,
            'reviews' => $bot->unreadReviews
        ]);
    }
}
