<?php

namespace app\modules\bot\models;

use Yii;

/**
 * This is the model class for table "{{%platforms}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $token
 * @property string $token_botan
 * @property int $bot_id
 * @property int $platform_id
 *
 * @property Bots $bot
 */
class Platform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%platforms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'bot_id', 'platform_id'], 'required'],
            [['description'], 'string'],
            [['bot_id', 'platform_id'], 'integer'],
            [['name', 'token', 'token_botan'], 'string', 'max' => 255],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'token' => 'Token',
            'token_botan' => 'Token Botan',
            'bot_id' => 'Bot ID',
            'platform_id' => 'Platform ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }



    public static function createTelegramIntegration($arBot)
    {
        $bot = new Bot();
        $bot->name = $arBot['first_name'];
        $bot->save(false);

        $integration = new self();
        $integration->name = $arBot['username'];
        $integration->platform_id = 1;
        $integration->bot_id = $bot->id;
        $integration->save(false);

        return $integration;
    }

    public static function getTelegramIntegration($arBot)
    {
        $integration = self::findOne(['name' => $arBot['username'], 'platform_id' => 1]);

        if(!$integration){
            $integration = self::createTelegramIntegration($arBot);
        }

        return $integration;
    }
}
