<?php

namespace app\modules\bot\models;

use Yii;
use app\modules\review\models\Review;
use app\modules\bot\models\UserBot;
use app\modules\bot\models\Bot;
use app\modules\webhooks\models\TelegramButtons;
use app\modules\webhooks\models\TelegramMessageButtons;
use app\modules\socket\models\SocketConnect;

/**
 * This is the model class for table "{{%messages}}".
 *
 * @property int $id
 * @property string $text
 * @property string $message_id
 * @property string $action
 * @property string $date
 * @property int $is_bot
 * @property int $user_bot_id
 * @property int $review_id
 * @property int $is_read
 *
 * @property Reviews $review
 * @property UsersBot $userBot
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%messages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['user_bot_id', 'review_id'], 'integer'],
            [['message_id', 'action'], 'string', 'max' => 255],
            [['is_bot'], 'integer', 'max' => 1],
            [['review_id'], 'exist', 'skipOnError' => true, 'targetClass' => Review::className(), 'targetAttribute' => ['review_id' => 'id']],
            [['user_bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserBot::className(), 'targetAttribute' => ['user_bot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'message_id' => 'Message ID',
            'action' => 'Action',
            'date' => 'Date',
            'is_bot' => 'Is Bot',
            'user_bot_id' => 'User Bot ID',
            'review_id' => 'Review ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReview()
    {
        return $this->hasOne(Review::className(), ['id' => 'review_id']);
    }

    public function getButtons()
    {
        $TMBtn = TelegramMessageButtons::tableName();
        $TBtn = TelegramButtons::tableName();

        return (new \yii\db\Query())
            ->select([$TMBtn.'.id', $TBtn.'.text', $TBtn.'.callback_json'])
            ->from($TMBtn)
            ->where(['message_id' => $this->id])
            ->leftJoin($TBtn, $TBtn.'.id='.$TMBtn.'.button_id')->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserBot()
    {
        return $this->hasOne(UserBot::className(), ['id' => 'user_bot_id']);
    }

    public function getDatestr()
    {
        $m = [1 => 'января',2 => 'февраля',3 => 'марта',4 => 'апреля',5 => 'мая',6 => 'июня',7 => 'июля',8 => 'августа',9 => 'сентября',10 => 'октября',11 => 'ноября',12 => 'декабря'];
        $date = getdate(strtotime($this->date));
        return $date['mday'].' '.$m[$date['mon']].' '.$date['year'].', '.$date['hours'].':'.$date['minutes'];
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $userBotTable = UserBot::tableName();
        $botTable = Bot::tableName();
        $res = (new \yii\db\Query())
            ->select($botTable.'.user_id AS u_id')
            ->from($botTable)
            ->rightJoin($userBotTable, $botTable.'.id = '.$userBotTable.'.bot_id')
            ->where($userBotTable.'.id = '.$this->user_bot_id)
            ->one();
            
        $user = $this->userBot;
        $connect = ($this->action == 'connect');
        if ($connect) {
            $user->connect = 1;
            $user->save();
        }

        if ($this->review_id) {
            $count = self::find()->where(['review_id' => $this->review_id])->count();
        } else {
            $count = self::find()->where(['user_bot_id' => $this->user_bot_id])->count();
        }

        SocketConnect::send(
            array(
                array(
                    'channel_id' => (int)$res['u_id'],
                    'data' => [
                        'message_id' => $this->id,
                        'user_id' => $this->user_bot_id,
                        'review_id' => $this->review_id,
                        'is_connect' => $connect,
                        'count' => $count,
                        'connect' => $user->connect,
                    ]
                )
            )
        );
    }
}
