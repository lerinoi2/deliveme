<?php

namespace app\modules\bot\models;

use Yii;
use app\modules\review\models\Review;

/**
 * This is the model class for table "{{%bots}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $user_id
 *
 * @property Users $user
 * @property Platforms[] $platforms
 * @property UsersBot[] $usersBots
 */
class Bot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bots}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_id'], 'required'],
            [['description'], 'string'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatforms()
    {
        return $this->hasMany(Platform::className(), ['bot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersBots()
    {
        return $this->hasMany(UserBot::className(), ['bot_id' => 'id']);
    }

    public function getUnreadMessages()
    {
        $mess_table = Message::tableName();
        $user_table = UserBot::tableName();
        return (new \yii\db\Query())
            ->from($mess_table)
            ->rightJoin($user_table, $user_table.'.id = '.$mess_table.'.user_bot_id')
            ->where([$mess_table.'.is_read' => 0, $mess_table.'.is_bot' => 0, $user_table.'.bot_id' => $this->id])
            ->count();
    }

    public function getUnreadReviews()
    {
        $mess_table = Message::tableName();
        $user_table = UserBot::tableName();
        $reviews_table = Review::tableName();
        return (new \yii\db\Query())
            ->select([$reviews_table.'.id as group'])
            ->from($reviews_table)
            ->leftJoin($mess_table, $mess_table.'.review_id = '.$reviews_table.'.id')
            ->leftJoin($user_table, $user_table.'.id = '.$mess_table.'.user_bot_id')
            ->where([$user_table.'.bot_id' => $this->id, $reviews_table.'.status' => 0])
            ->groupBy('group')
            ->count();
    }
}
