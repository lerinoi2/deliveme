<?php

namespace app\modules\bot\models;

use Yii;
use app\modules\webhooks\models\TelegramApi;
use app\modules\webhooks\models\TelegramMessageButtons;
use app\modules\webhooks\models\TelegramButtons;
use app\modules\webhooks\models\ViberButtons;
use app\modules\webhooks\models\ViberApi;
use app\modules\webhooks\models\Files;
use app\modules\review\models\Review;

/**
 * This is the model class for table "{{%users_bot}}".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property int $platform
 * @property string $chat_id
 * @property int $bot_id
 * @property int $connect
 * @property int $check
 *
 * @property Messages[] $messages
 * @property Reviews[] $reviews
 * @property Bots $bot
 */
class UserBot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users_bot}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'platform', 'chat_id', 'bot_id'], 'required'],
            [['platform', 'bot_id'], 'integer'],
            [['name', 'image', 'chat_id'], 'string', 'max' => 255],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'image' => 'Аватар',
            'platform' => 'Платформа',
            'chat_id' => 'Ид чата',
            'bot_id' => 'Ид бота',
            'phone' => 'Телефон',
            'reviews' => 'Отзывы',
            'reviewsCount' => 'Отзывы',
            'orders' => 'Заказы',
        ];
    }
    const PLATFORM = [
        'telegram' => 0,
        'viber' => 1,
        'vk' => 2
    ];
    const PlatformName = [
        0 => 'telegram',
        1 => 'viber',
        2 => 'vk'
    ];
    public static function getPArray()
    {
        return array(
            self::PLATFORM['telegram'] => 'Телеграмм',
            self::PLATFORM['viber'] => 'Вайбер',
        );
    }
    public function getPlatformName()
    {
        return self::PlatformName[$this->platform];
    }

    public function getTelegramMessagesWithButtons($filter = array()){
        $tn = Message::tableName();
        $TMBtn = TelegramMessageButtons::tableName();
        $TBtn = TelegramButtons::tableName();

        $select = array();
        $attrs = Message::attributes();
        foreach ($attrs as $attrName){
            array_push($select,$tn.'.'.$attrName);
        }

        $btnAttrs = TelegramButtons::attributes();
        foreach ($btnAttrs as $btnAttr){
            $select['btn_'.$btnAttr] = $TBtn.'.'.$btnAttr;
        }

        $filter['user_bot_id'] = $this->id;
        $allRows = (new \yii\db\Query())
            ->select($select)
            ->from($tn)
            ->where($filter)
            ->leftJoin($TMBtn, $TMBtn.'.message_id='.$tn.'.id')
            ->leftJoin($TBtn, $TBtn.'.id='.$TMBtn.'.button_id')->all();

        $msgById = array();
        $btnTextByAction = array();
        foreach($allRows as $row){
            if(!$msgById[$row['id']]){
                $msgById[$row['id']] = $row;
            }

            if($row['btn_id']){
                $arBtn = array(
                    'id' => $row['btn_id'],
                    'callback_json' => $row['btn_callback_json'],
                    'text' => $row['btn_text'],
                );

                $btnTextByAction[$row['btn_callback_json']] = $row['btn_text'];

                if(is_array($msgById[$row['id']]['buttons'])){
                    array_push($msgById[$row['id']]['buttons'], $arBtn);
                }else{
                    $msgById[$row['id']]['buttons'] = array($arBtn);
                }
            }
        }

        foreach($msgById as $id => $msg){
            if($msg['action'] === 'pressButton'){
                $msgById[$id]['text'] = $btnTextByAction[$msg['text']];
            }
        }

        return $msgById;
    }

    public function getVKMessagesgetViberMessagesWithButtons($filter = array()){
        $filter['user_bot_id'] = $this->id;
        $msg = Message::find()->where($filter)->indexBy('id')->all();
        $res = array();
        foreach ($msg as $message){
            $arButtons = array();
            $buttons = ViberButtons::find()->where(['message_id' => $message->id])->all();
            foreach ($buttons as $btn){
                array_push($arButtons, $btn->toArray());
            }

            $res[$message->id] = array_merge($message->toArray(), array('buttons' => $arButtons));
        }
        
        return $res;
    }
    public function getViberMessagesWithButtons($filter = array()){
        $filter['user_bot_id'] = $this->id;
        $msg = Message::find()->where($filter)->indexBy('id')->all();
        $res = array();
        foreach ($msg as $message){
            $arButtons = array();
            $buttons = ViberButtons::find()->where(['message_id' => $message->id])->all();
            foreach ($buttons as $btn){
                array_push($arButtons, $btn->toArray());
            }

            $res[$message->id] = array_merge($message->toArray(), array('buttons' => $arButtons));
        }

        return $res;
    }

    public function getVKMessages($filter = array()){
        $filter['user_bot_id'] = $this->id;
        $msg = Message::find()->where($filter)->indexBy('id')->asArray()->all();

        return $msg;
    }

    public function getReviewMessages($id){
        switch($this->platform){
            case 0:
                return $this->getTelegramMessagesWithButtons(array('review_id' => $id));
            case 1:
                return $this->getViberMessagesWithButtons(array('review_id' => $id));
            case 2:
                return $this->getVKMessages(array('review_id' => $id));
            default:
                return array();
        }
    }

    public function getMessagesWithButtons($filter = array())
    {
        switch($this->platform){
            case 0:
                return $this->getTelegramMessagesWithButtons($filter);
            case 1:
                return $this->getViberMessagesWithButtons($filter);
            case 2:
                return $this->getVKMessages($filter);
            default:
                return array();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['user_bot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['user_bot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    public function getLastMessage()
    {
        return $this->hasMany(Message::className(), ['user_bot_id' => 'id'])
            // ->where(['is_bot' => 0])
            ->select(['date', 'action'])
            ->orderBy('date DESC')
            ->one();
    }

    public function getReviewsCount()
    {
        return $this->hasMany(Message::className(), ['user_bot_id' => 'id'])
            ->where(['not', ['review_id' => null]])
            ->groupBy('review_id')
            ->count();
    }

    public static function createUser($arUser, $bot_id, $platform)
    {
        $user = new self();
        $user->chat_id = $arUser['id'];
        $user->platform = $platform;

        if($platform == 0){ //Check telegram users
            $user->name = $arUser['first_name'].' '.$arUser['last_name'];
        }else if($platform == 1){ //Check viber users
            $user->name = $arUser['name'] ? $arUser['name'] : 'Инкогнито';
            $user->image = $arUser['avatar'];
        }else if($platform == 2){ //VK users
            $user->name = $arUser['id'];
        }

        $user->bot_id = $bot_id;
        $user->date = time();
        $user->save(false);

        return $user;
    }

    public function savePhoto()
    {
        if($this->platform === 0){
            $tgConnect = new TelegramApi($this->id);
            $this->image = $tgConnect->saveUserImage();
            $this->save(false);
        }else{
            $viberConnect = new ViberApi($this->id);
            $this->image = $viberConnect->saveUserImage();
            $this->save(false);
        }

        return $this->image;
    }

    public function addOrder()
    {
        $this->orders = $this->orders + 1;
        $this->save();
    }

    public function savePhoneNumber($phone)
    {
        $phone = str_replace("+7", "8", $phone);
        $phone = preg_replace('/(\(|\)|-|\s|\s+)/', '', $phone);
        if(!strlen($phone) || !(int)$phone) return false;
        $this->phone = $phone;
        return $this->save();
    }

    public static function getUser($arUser, $bot_id, $platform)
    {
        $user = self::findOne(['chat_id' => $arUser['id'], 'bot_id' => $bot_id]);
        if(!$user){
            $user = self::createUser($arUser, $bot_id, $platform);
        }
        return $user;
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            $this->savePhoto();
        }
    }

    public function getLetters()
    {
        $letters = '';
        foreach (explode(' ', $this->name) as $word){
            $letters .= (mb_substr($word, 0, 1));
        }
        return $letters;
    }

    public function getUnreadMessages()
    {
        return $this->hasMany(Message::className(), ['user_bot_id' => 'id'])
            ->where(['is_read' => 0, 'is_bot' => 0])
            ->count();
    }

    public function getUnreadReviews()
    {
        $mess_table = Message::tableName();
        $rows = (new \yii\db\Query())
            ->select([$mess_table.'.review_id'])
            ->from($mess_table)
            ->where(['user_bot_id' => $this->id])
            ->andWhere(['not', ['review_id' => null]])
            ->indexBy('review_id')
            ->all();
        return Review::find()->where(['status' => 0, 'id' => array_keys($rows)])->count();
    }

    public function closeChat()
    {
        $this->connect = 0;
        return $this->save();
    }
}
