<?
if (is_array($messages)) {
    foreach ($messages as $message) {
?>
    <?= $this->render('_message', [
        'message' => $message,
        'platform' => $platform,
    ]) ?>
<?
    }
}
?>