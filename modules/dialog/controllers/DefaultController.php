<?php

namespace app\modules\dialog\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bot\models\Bot;
use app\modules\bot\models\UserBot;
use app\modules\bot\models\Message;
use app\modules\webhooks\models\TelegramApi;
use app\modules\webhooks\models\ViberApi;

/**
 * Default controller for the `dialog` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest && $action->id != 'checkconn') {
            $this->redirect('/');
            return false;
        }
        return true;
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id)
    {
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;

        $query = UserBot::find()->where(['bot_id' => $bot_id]);
        $resUsers = array();
        // $resMessages = array();
        $search = Yii::$app->request->get('search');
        if ($search) {
            $messages = Message::find()->where(['like','text',$search])->all();
            foreach ($messages as $message) {
                $resUsers[] = $message->user_bot_id;
                // $resMessages[] = $message->id;
            }

            $query->andWhere(['or', ['like', 'name', $search], ['id' => $resUsers]]);
        }

        $users = $query->indexBy('id')->all();

        if ($id) {
            $messages = $users[$id]->messagesWithButtons;
            Message::updateAll(['is_read' => 1], ['user_bot_id' => $id, 'is_read' => 0]);
            UserBot::updateAll(['check' => time()], ['id' => $id]);
        }

        $lastMessages = array();
        if (is_array($users)) {
            foreach ($users as $user) {
                $lastMessages[$user->id] = $user->lastMessage;
            }
            
            // Сортировка пользователей по дате последнего сообщения.
            uasort($users, function ($a, $b) use($lastMessages) {
                if ($b->connect == $a->connect) {
                    return strtotime($lastMessages[$b->id]['date']) - strtotime($lastMessages[$a->id]['date']);
                }
                return ($b->connect < $a->connect)? -1 : 1;
            });
        }

        return$this->render('index',[
            'users' => $users, 
            'id' => $id, 
            'messages' => $messages,
            'lastMessages' => $lastMessages
            // 'resMes' => $resMessages,
        ]);
    }

    public function actionMute($id)
    {
        $user = UserBot::findOne($id);
        $user->muted = !$user->muted;
        $user->save(false);
        return $this->redirect(Yii::$app->request->referrer);
        if ($user->save(false)) {
            return true;
        }
        return false;
    }

    public function actionUser($id)
    {
        $user = UserBot::findOne($id);
        return $this->renderPartial('_user', [
            'user' => $user,
            'lastMessage' => $user->lastMessage,
            'is_cur' => ($user->id == Yii::$app->request->get('user_id')),
        ]);
    }

    // */5 * * * * wget -O /dev/null http://lk.delive.me/dialog/checkconn
    public function actionCheckconn()
    {
        $users = UserBot::find()->where(['connect' => 2])->all();
        $dateMax = time() - 1800;
        foreach ($users as $user) {
            $dateMes = strtotime($user->lastMessage->date);
            if ($dateMes < $dateMax) {
                if ($user->platform == UserBot::PLATFORM['telegram']) {
                    $api = new TelegramApi($user->id);
                } elseif ($user->platform == UserBot::PLATFORM['viber']) {
                    $api = new ViberApi($user->id);
                }
                $api->sendMessage('Оператор был отключен от диалога в связи с отсутствием активности в чате. Если вы хотите, чтобы оператор снова подключился к чату, нажмите на кнопку "Чат с оператором".');
                $api->enableChat();

                UserBot::updateAll(['connect' => 0], ['id' => $user->id]);
                $res[] = $user->id;
            }
        }
        // return '<pre>' . print_r($res, true) . '</pre>';
        return json_encode($res);
    }
}
