<?
$this->title = 'Диалоги';

$platformName = $users[$id]->platformName;

?>
<div class="col s6 l5 xxm4 xxl3 grey lighten-3 wrapper-container">
  <div class="section">
    <div class="row m0">
      <div class="col s10">
        <div class="input-field m0">
          <form action="">
            <input name="search" type="search" placeholder="Поиск" class="m0 p10" value="<?=$_GET['search']?>">
          </form>
        </div>
      </div>
      <div class="col s2 flex-btn">
        <!-- <i class="material-icons small">volume_up</i> -->
        <i class="material-icons small reset">close</i>
      </div>
    </div>
  </div>
  <div class="divider"></div>
  <div class="section wrapper-section">
    <div class="wrapper users">
      <?= $this->render('_users', [
          'users' => $users,
          'lastMessages' => $lastMessages,
          'id' => $id,
      ]) ?>
    </div>
  </div>
</div>
<div class="col s6 l7 xxm8 xxl9 grey lighten-4 wrapper-container">
  <?if ($id) {?>
  <div class="section white z-depth-2">
    <div class="row m0">
      <div class="col s7 xxm9">
        <div class="users__img">
          <div class="img <?=$users[$id]->image ? 'fz0' : ' no_image color'.(strlen($users[$id]->name)%5) ?>" style="background-image: url(<?=$users[$id]->image?>)">
              <? 
                $arName = explode(' ', $users[$id]->name);
                foreach ($arName as $word){
                    echo(mb_substr($word, 0, 1));
                }
              ?>
          </div>
          <div class="img-label img-label-<?=$users[$id]->platform?>"></div>
        </div>
        <div class="flow-text truncate"><?=$users[$id]->name?></div>
        <div class="big-text"><?=($users[$id]->phone?'Телефон: '.$users[$id]->phone:'Телефон не указан')?></div>
      </div>
      <div class="col s5 xxm3 flex-btn mt10">
        <a class="btn" href="/review?search=<?=str_replace(' ', '+', $users[$id]->name)?>">Отзывы (<?=$users[$id]->reviewsCount?>)</a>

        <!-- <i class="material-icons small link tooltipped" data-section="dialog/mute" data-id="<?=$id?>" data-position="bottom" data-tooltip="<?=$users[$id]->muted?'Отключить':'Включить'?> уведомления"><?=$users[$id]->muted?'block':'volume_up'?></i> -->
      </div>
    </div>
  </div>
  <?}?>

  <div class="section wrapper-section wrapper-section_messages messages_<?=$platformName?>">
    <div class="wrapper messages">
      <?= Yii::$app->view->renderFile('@app/modules/bot/views/default/_messages.php', [
          'platform' => $users[$id]->platformName,
          'messages' => $messages,
      ]) ?>
    </div>
  </div>

  <?if ($id) {?>
  <form class="ajaxForm userConnectBlock section white <?=($users[$id]->connect == 2?'connected':'')?>" action="/api/<?=$platformName?>/send_message/<?=$id?>" method="post">
    <div class="row m0">
      <div class="col s7 xxm9">
        <div class="input-field m0">
          <input type="text" placeholder="Введите сообщение" name="text" class="m0">
        </div>
      </div>
      <div class="col s5 xxm3 flex-btn">
        <!--<i class="material-icons small">person</i>
        <i class="material-icons small">close</i>
        <i class="material-icons small">block</i>
        <i class="material-icons small">mood</i>-->
        <!-- <button><i class="material-icons small">send</i></button> -->
        <i class="material-icons small sendBtn">send</i>
        <div class="btn userConnect" href="/api/<?=$platformName?>/enable/<?=$id?>">Отключиться</div>
      </div>
    </div>
    <div class="connectBlock">
      <div class="btn userConnect" href="/api/<?=$platformName?>/disable/<?=$id?>">Подключиться к диалогу</div>
    </div>
  </form>
  <?}?>
</div>

<script>
  global_userBotID = <?=$id?$id:'false'?>;
</script>
