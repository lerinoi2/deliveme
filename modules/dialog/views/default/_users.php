<?php

if (count($users) > 0) {
  foreach ($users as $user) {
?>
    <?= $this->render('_user', [
        'user' => $user,
        'lastMessage' => $lastMessages[$user->id],
        'is_cur' => ($user->id == $id),
    ]);
?>
<?php
  }
} else {
  echo '<div class="flow-text center-align">Ничего не найдено</div>';
}