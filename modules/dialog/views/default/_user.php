<div 
data-section="dialog" 
data-id="<?=$user->id?>" 
data-date="<?=strtotime($lastMessage->date)?>"
data-connect="<?=$user->connect?1:0?>"
class="card-panel hoverable users__card link <?=$is_cur?'bg-0':''?>">
  <div class="users__img">
    <div class="img <?=$user->image ? 'fz0' : ' no_image color'.(strlen($user->name)%5) ?>" style="background-image: url(<?=$user->image?>)">
        <? 
          $arName = explode(' ', $user->name);
          foreach ($arName as $word){
              echo(mb_substr($word, 0, 1));
          }
        ?>
    </div>
    <div class="img-label img-label-<?=$user->platform?>"></div>
  </div>
  <div class="big-text truncate"><?=$user->name?></div>
  <div class="middle-text">
    <?=$lastMessage->datestr?>
    <?
      $countUM = $user->unreadMessages;
      if ($countUM) {
        echo '<span class="new badge red">'.$countUM.'</span>';
      }
    ?>
  </div>
  <div class="small-text"><?=$lastMessage->action?></div>
  <div class="<?=$user->connect == 2?'dot':($user->connect == 1?'dot puls':'')?>"></div>
</div>