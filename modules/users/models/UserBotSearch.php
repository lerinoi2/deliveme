<?php

namespace app\modules\users\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\bot\models\UserBot;
use app\modules\bot\models\Bot;
use app\modules\bot\models\Message;

/**
 * UserBotSearch represents the model behind the search form of `app\modules\bot\models\UserBot`.
 */
class UserBotSearch extends UserBot
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'platform', 'bot_id', 'connect', 'date', 'active_review_id', 'orders', 'check'], 'integer'],
            [['name', 'image', 'chat_id', 'phone', 'muted', 'reviewsCount'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserBot::find();
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
        $query->where(['bot_id' => $bot_id]);
        // add conditions that should always apply here

        // $subQuery = Message::find()
        //     ->select('review_id, user_bot_id, COUNT(DISTINCT review_id) as countReviews')
        //     ->where(['not', ['review_id' => null]])
        //     ->groupBy('review_id');
        // $query->leftJoin(['mes' => $subQuery], 'mes.user_bot_id = id');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name',
                'platform',
                'orders',
                'phone',
                // 'reviewsCount' => [
                //     'asc' => ['mes.countReviews' => SORT_ASC],
                //     'desc' => ['mes.countReviews' => SORT_DESC],
                // ]
            ]
        ]);  

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'platform' => $this->platform,
            'bot_id' => $this->bot_id,
            'connect' => $this->connect,
            'date' => $this->date,
            'active_review_id' => $this->active_review_id,
            'orders' => $this->orders,
            'check' => $this->check,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'chat_id', $this->chat_id])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'muted', $this->muted]);

        return $dataProvider;
    }
}
