<?
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Пользователи';
?>
<div class="col s12 wrapper">
<?
$columns = [
    // 'id',
    [
        'attribute' => 'image',
        'headerOptions' => ['style' => 'width:70px;'],
        'format' => 'html',
        'value' => function ($data){
            return '<div class="users__img m0"><div class="img '.($data['image'] ? 'fz0' : ' no_image color'.(strlen($data['name'])%5)).'" style="background-image: url('.$data['image'].')">'.$data->letters.'</div></div>';
        },
    ], 
    'name',
    [
        'attribute' => 'platform',
        'headerOptions' => ['style' => 'width:20px;'],
        'format' => 'html',
        'value' => function ($data){
            return '<div class="img-label-static img-label-'.$data['platform'].'"></div>';
        },
    ], 
    // 'chat_id',
    'phone',
    'orders',
    'reviewsCount',
    // [
    //     'attribute' => 'reviews',
    //     'headerOptions' => ['style' => 'width:100px;'],
    //     'format' => 'html',
    //     'value' => function ($data){
    //         return $data['reviewsCount'];
    //         // return $data->reviewsCount;
    //     },
    // ], 
]
?>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-'],
    'layout'=>"{items}\n{pager}",
    'tableOptions' => [
        'class' => 'highlight centered'
    ],
    'emptyText' => 'Нет пользователей',
    'pager' => [
        'pageCssClass' => 'waves-effect',
        'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
        'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
        // 'activePageCssClass' => 'def-bg',
        'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'disabled']
    ],
    'columns' => $columns,
]); ?>
<?php Pjax::end(); ?>
</div>