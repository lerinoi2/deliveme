<?php

namespace app\modules\users\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bot\models\UserBot;
use app\modules\users\models\UserBotSearch;

/**
 * Default controller for the `users` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
            return false;
        }
        return true;
    }
    /**
     * Lists all UserBot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserBotSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 7];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
