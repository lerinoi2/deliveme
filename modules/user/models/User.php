<?php

namespace app\modules\user\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property int $role
 * @property string $phone
 * @property string $email
 * @property int $confirm
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{    
    public static function tableName()
    {
        return '{{%users}}';
    }

    public function rules()
    {
        return [
            [['login', 'password', 'role'], 'required'],
            [['role'], 'integer'],
            [['login', 'password', 'phone', 'email'], 'string', 'max' => 255],
            [['confirm'], 'string', 'max' => 1],
            [['login'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'role' => 'Role',
            'phone' => 'Phone',
            'email' => 'Email',
            'confirm' => 'Confirm',
        ];
    }
    
    public $auth_token;
    public $_password;
    const ROLE = [
        'ADMIN' => 0,
        'MANAGER' => 1,
    ];

    public static function getRolesArray()
    {
        return array(
            self::ROLE['ADMIN'] => 'Директор',
            self::ROLE['MANAGER'] => 'Менеджер',
            self::ROLE['BOOKER'] => 'Бухгалтер',
            self::ROLE['MARKETER'] => 'Маркетолог',
            self::ROLE['CAMP'] => 'Лагерь',
        );
    }

    public static function findIdentity($id)
    {
        return self::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }


    public function getRoleName()
    {
        $roleArr = $this->getRolesArray();
        return $roleArr[$this->role];
    }

    public function getRoleId($name)
    {
        $roleArr = $this->getRolesArray();
        return array_search($name, $roleArr);
    }

    public static function myRole()
    {
        return Yii::$app->user->identity->role;
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_token;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_token === $authKey;
    }

    public static function findByUsername($username){
        return static::findOne(['login' => $username]);
    }

    public function validatePassword($password){
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function setPassword($password){
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public static function findByPasswordResetToken($token)
    {
        return static::findOne([
            'reset' => $token
        ]);
    }
    public function generatePasswordResetToken()
    {
        $this->reset = Yii::$app->security->generateRandomString() . '_' . time();
    }
    public function removePasswordResetToken()
    {
        $this->reset = null;
    }
}
