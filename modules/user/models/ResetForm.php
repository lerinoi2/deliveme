<?php
namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;

/**
 * Password reset request form
 */
class ResetForm extends Model
{
    public $login;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'filter', 'filter' => 'trim'],
            ['login', 'required', 'message' => 'Не может быть пустым'],
            // ['login', 'email', 'message' => 'Логин должен быть email'],
            ['login', 'exist',
                'targetClass' => User::className(),
                'message' => 'Такого E-mail не найдено'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */

    public function sendReset()
    {
        $user = User::findByUsername($this->login);
        
        $user->generatePasswordResetToken();
        
        if (!$user->save()) {
            return false;
        }

        return Yii::$app
            ->mailer
            ->compose(
                '@app/mail/passwordReset',
                ['user' => $user]
            )
            ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->params['projectName']])
            ->setTo($this->login)
            ->setSubject('Сброс пароля на сайте '.Yii::$app->params['projectName'])
            ->send();
    }
}
