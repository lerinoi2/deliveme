<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;
use app\modules\user\models\User;
use app\modules\user\models\LoginForm;
use app\modules\user\models\ResetForm;
use app\modules\user\models\ResetPasswordForm;

/**
 * SignupController controller for the `User` module
 */
class SignupController extends Controller
{
    public function beforeAction($action)
    {   
        if (!Yii::$app->user->isGuest && $action->id != 'logout') {
            $this->redirect('/');
            return false;
        }
        return true;
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionReguser()
    {
        $model = new User();
        $model->login = 'password';
        $model->phone = '89024792373';
        $model->email = 'm@ckdigital.ru';
        $model->role = User::ROLE['ADMIN'];
        $model->setPassword('WqTlZ9HZ');
        $model->save();
        print('ok');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect('/');
    }

    public function actionReset()
    {
        $model = new ResetForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendReset()) {
                return $this->redirect('/login');
            }
        }
        return $this->render('reset', [
            'model' => $model
        ]);
    }

    public function actionPasswordReset($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage()); // TODO ERROR PASSWORD ALREADY RESET
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            return $this->redirect('/login');
        }

        return $this->render('resetForm', [
            'model' => $model
        ]);
    }
}