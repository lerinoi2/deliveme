<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;

/**
 * Default controller for the `user` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action)
    {   
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}
