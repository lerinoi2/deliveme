<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Смена пароля';
?>

<div class="col s12 center-align">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'card left-align'],
        'fieldConfig' => [
            'template' => "{label}\n{input}\n{error}"
        ],
    ]); ?>
        <div class="card-content">
            <span class="card-title">Смена пароля</span>
            <?= $form->field($model, 'password')->textInput(['autofocus' => true])->label('Пароль')->passwordInput() ?>
            <?= $form->field($model, 'passwordRepeat')->textInput()->label('Повтор')->passwordInput() ?>
        </div>
        <div class="card-action">
            <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            <?= Html::a('Вернуться', ['/login'], ['class' => 'btn btn-link']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
