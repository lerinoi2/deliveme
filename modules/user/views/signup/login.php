<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    // 'options' => ['class' => ' '],
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}"
    ],
]); ?>
    <div class="card-content">
        <span class="card-title center-align">Вход в систему</span>
        <?= $form->field($model, 'username', ['options'=>['class'=>'input-field']])->textInput(['autofocus' => true])?>

        <?= $form->field($model, 'password', ['options'=>['class'=>'input-field']])->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "{input} {label}\n{error}",
        ])->label('Запомнить') ?>
    </div>
    <div class="card-action">
        <?//= Html::a('Забыли пароль?', ['/reset'], ['class' => 'btn btn-flat def-color']) ?>
        <?= Html::submitButton('Войти', ['class' => 'btn def-bg right', 'name' => 'login-button']) ?>
    </div>
<?php ActiveForm::end(); ?>
