<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление пароля';
?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    // 'options' => ['class' => 'card left-align'],
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}"
    ],
]); ?>
    <div class="card-content">
        <span class="card-title">Восстановление пароля</span>
        <?= $form->field($model, 'login')->textInput(['autofocus' => true])->label('Логин') ?>
    </div>
    <div class="card-action">
        <?= Html::submitButton('Восстановить', ['class' => 'btn', 'name' => 'login-button']) ?>
        <?= Html::a('Вернуться', ['/login'], ['class' => 'btn btn-link']) ?>
    </div>
<?php ActiveForm::end(); ?>
