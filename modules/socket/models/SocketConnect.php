<?php
namespace app\modules\socket\models;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;

require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';


class SocketConnect{
    private function call($data)
    {
        if(!is_array($data)) return 'Error: Incorrect input format';

        $client = new Client(new Version2X('http://127.0.0.1:3000'));
        $client->initialize();
        foreach ($data as $msg){
            $client->emit('chat message', ['roomID' => $msg['channel_id'], 'message' => $msg['data']]);
        }
        $client->close();

        return 'done';
    }
    public static function send(array $data = array())
    {
        return self::call($data);
    }
}
