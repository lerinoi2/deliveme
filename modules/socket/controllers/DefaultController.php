<?php

namespace app\modules\socket\controllers;

use Yii;
use yii\web\Controller;
use app\modules\socket\models\SocketConnect;
use app\console\controllers\SocketController;


class DefaultController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    //infinite process
    //need call with timeout
    public static function actionStart()
    {
        $status = SocketConnect::check();

        if($status === 'ok'){
            return 'Server already is running';
        }

        ob_end_clean();
        header("Connection: close\r\n");
        header("Content-Encoding: none\r\n");
        ignore_user_abort(true); // optional
        ob_start();

        echo ($status);
        $size = ob_get_length();
        header("Content-Length: $size");
        ob_end_flush();     // Strange behaviour, will not work
        flush();            // Unless both are called !
        ob_end_clean();
        session_write_close();
        //do processing here

        SocketController::actionStartSocket();
    }
    //action to run socket server with broke connection
    public static function actionRunSocket()
    {
        return SocketConnect::send(array('roomID' => 1, 'message' => 'test text'));
        // this make request for 3 second to /modules/socket/controllers/defaultController/actionStart
        //return SocketConnect::runServer();
    }
}
