<?php

namespace app\modules\statistics\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bot\models\Bot;
use app\modules\bot\models\UserBot;
use app\modules\bot\models\Message;
use app\modules\review\models\Review;

/**
 * Default controller for the `statistics` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $filter = Yii::$app->request->get();
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
        $users = UserBot::find()->where(['bot_id' => $bot_id])->indexBy('id');
        if (is_array($filter['Platforms'])) {
            $pFilter = array();
            foreach ($filter['Platforms'] as $key => $value) {
                $pFilter[] = UserBot::PLATFORM[$key];
            }
            $users->andWhere(['platform' => $pFilter]);
        }
        $users = $users->all();

        $usersIds = array_keys($users);

        if ($filter['date_start'] || $filter['date_end']) {
            $usersNew = UserBot::find()->where(['bot_id' => $bot_id]);
            if ($filter['date_start']) {
                $usersNew->andWhere(['>=','date', strtotime($filter['date_start'])]);
            }
            if ($filter['date_end']) {
                $usersNew->andWhere(['<=','date', strtotime($filter['date_end'])]);
            }
            $usersNew = $usersNew->all();
            // echo "<pre>";
            // print_r($usersNew);
            // echo "</pre>";
        } else {
            $usersNew = $users;
        }

        $messagesGet = Message::find()->where(['user_bot_id' => $usersIds, 'is_bot' => 0])->indexBy('id');
        if ($filter['date_start']) {
            $messagesGet->andWhere(['>=','date', date("Y-m-d H:i:s", strtotime($filter['date_start']))]);
        }
        if ($filter['date_end']) {
            $messagesGet->andWhere(['<=','date', date("Y-m-d H:i:s", strtotime($filter['date_end']))]);
        }
        $messagesGet = $messagesGet->all();

        $messagesSend = Message::find()->where(['user_bot_id' => $usersIds, 'is_bot' => 1])->indexBy('id');
        if ($filter['date_start']) {
            $messagesSend->andWhere(['>=','date', date("Y-m-d H:i:s", strtotime($filter['date_start']))]);
        }
        if ($filter['date_end']) {
            $messagesSend->andWhere(['<=','date', date("Y-m-d H:i:s", strtotime($filter['date_end']))]);
        }
        $messagesSend = $messagesSend->all();

        $messagesGetIds = array_keys($messagesGet);

        $mwr = Message::find()
            ->where(['id' => $messagesGetIds])
            ->andWhere(['not', ['review_id' => null]])
            ->groupBy('review_id')
            ->indexBy('review_id')
            ->all();
        $reviews = Review::find()->where(['id' => array_keys($mwr)])->all();


        // сохранение фильтров
        $params = [
            'date_start' => $filter['date_start'],
            'date_end' => $filter['date_end'],
            'telegram' => isset($filter['Platforms'])?($filter['Platforms']['telegram']?'checked':''):'checked',
            'viber' => isset($filter['Platforms'])?($filter['Platforms']['viber']?'checked':''):'checked',
        ];

        // массивы для графиков
        $usersChart = array();
        $usersNewChart = array();
        $messagesGetChart = array();
        $messagesSendChart = array();
        $reviewsChart = array();
        foreach ($users as $item) {
            $date = date("Y-m-d", $item->date);
            if (isset($usersChart[$date])) {
                $usersChart[$date]['count'] += 1;
            } else {
                $usersChart[$date] = [
                    'date' => $date,
                    'count' => 1,
                ];
            }
        }
        
        foreach ($usersNew as $item) {
            $date = date("Y-m-d", $item->date);
            if (isset($usersNewChart[$date])) {
                $usersNewChart[$date]['count'] += 1;
            } else {
                $usersNewChart[$date] = [
                    'date' => $date,
                    'count' => 1,
                ];
            }
        }
        
        foreach ($messagesGet as $item) {
            $date = date("Y-m-d", strtotime($item->date));
            if (isset($messagesGetChart[$date])) {
                $messagesGetChart[$date]['count'] += 1;
            } else {
                $messagesGetChart[$date] = [
                    'date' => $date,
                    'count' => 1,
                ];
            }
        }
        
        foreach ($messagesSend as $item) {
            $date = date("Y-m-d", strtotime($item->date));
            if (isset($messagesSendChart[$date])) {
                $messagesSendChart[$date]['count'] += 1;
            } else {
                $messagesSendChart[$date] = [
                    'date' => $date,
                    'count' => 1,
                ];
            }
        }

        foreach ($reviews as $item) {
            $date = date("Y-m-d", strtotime($mwr[$item->id]->date));
            if (isset($reviewsChart[$date])) {
                $reviewsChart[$date]['count'] += 1;
            } else {
                $reviewsChart[$date] = [
                    'date' => $date,
                    'count' => 1,
                ];
            }
        }
        
        $countOrders = 0;
        foreach ($users as $item) {
            $date = date("Y-m-d", $item->date);
            if (isset($ordersChart[$date])) {
                $ordersChart[$date]['count'] += $item->orders;
            } else {
                $ordersChart[$date] = [
                    'date' => $date,
                    'count' => $item->orders,
                ];
            }
            $countOrders += $item->orders;
        }

        return $this->render('index', [
            'params' => $params,

            'countUsers' => count($users),
            'countUsersNew' => count($usersNew),
            'countGetMes' => count($messagesGet),
            'countSendMes' => count($messagesSend),
            'countReviews' => count($reviews),
            'countOrders' => $countOrders,

            'users' => json_encode(array_values($usersChart)),
            'usersNew' => json_encode(array_values($usersNewChart)),
            'messagesGet' => json_encode(array_values($messagesGetChart)),
            'messagesSend' => json_encode(array_values($messagesSendChart)),
            'reviews' => json_encode(array_values($reviewsChart)),
            'orders' => json_encode(array_values($ordersChart)),
        ]);
    }
}
