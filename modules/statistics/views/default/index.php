<?
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Статистика';
?>
<div class="col s12 grey lighten-4 wrapper-container">
  <div class="section">
    <form class="valign-wrapper statsFilter">
      <div class="middle-text">Период:</div>
      <div class="stats__btns">
        <div class="btn waves-effect waves-light setFilterDate" data-value="today">Сегодня</div>
        <div class="btn waves-effect waves-light setFilterDate" data-value="yesterday">Вчера</div>
        <div class="btn waves-effect waves-light setFilterDate" data-value="week">Неделя</div>
        <div class="btn waves-effect waves-light setFilterDate" data-value="month">Месяц</div>
      </div>
      <div class="stats__inputs">
        <div class="date-field">
            <input type="text" name="date_start" placeholder="Дата от" value="<?=$params['date_start']?>">
        </div>
        <div class="date-field">
            <input type="text" name="date_end" placeholder="Дата до" value="<?=$params['date_end']?>">
        </div>
        <!-- <select name="">
          <option value="" disabled selected>Вид</option>
          <option value="1">По дням</option>
          <option value="2">По неделям</option>
          <option value="3">По месяцам</option>
        </select> -->
      </div>
      <div class="stats__switches">
        <div class="switch">
          <label>
            <span class="black-text">Telegram</span>
            <input type="checkbox" name="Platforms[telegram]" <?=$params['telegram']?> >
            <span class="lever"></span>
          </label>
        </div>
        <div class="switch">
          <label>
            <span class="black-text">Viber</span>
            <input type="checkbox" name="Platforms[viber]" <?=$params['viber']?>>
            <span class="lever"></span>
          </label>
        </div>
      </div>
    </form>
  </div>

  <div class="divider"></div>

  <div class="section wrapper-section">
    <div class="wrapper">
        <div class="row">
            <div class="col s3 xl2">
                <div class="card-panel statsCard hoverable">
                    <div class="statsTitle">Всего пользователей:</div>
                    <div class="divider"></div>
                    <div class="statsNum"><?=$countUsers?></div>
                </div>
            </div>
            <div class="col s3 xl2">
                <div class="card-panel statsCard hoverable">
                    <div class="statsTitle">Новых пользователей:</div>
                    <div class="divider"></div>
                    <div class="statsNum"><?=$countUsersNew?></div>
                </div>
            </div>
            <div class="col s3 xl2">
                <div class="card-panel statsCard hoverable">
                    <div class="statsTitle">Сообщений получено:</div>
                    <div class="divider"></div>
                    <div class="statsNum"><?=$countGetMes?></div>
                </div>
            </div>
            <div class="col s3 xl2">
                <div class="card-panel statsCard hoverable">
                    <div class="statsTitle">Сообщений отправлено:</div>
                    <div class="divider"></div>
                    <div class="statsNum"><?=$countSendMes?></div>
                </div>
            </div>
            <div class="col s3 xl2">
                <div class="card-panel statsCard hoverable">
                    <div class="statsTitle">Количество отзывов:</div>
                    <div class="divider"></div>
                    <div class="statsNum"><?=$countReviews?></div>
                </div>
            </div>
            <div class="col s3 xl2">
                <div class="card-panel statsCard hoverable">
                    <div class="statsTitle">Всего заказов:</div>
                    <div class="divider"></div>
                    <div class="statsNum"><?=$countOrders?></div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            // переменные для графиков
            var chart = {
                'users' : <?=$users?>,
                'newusers' : <?=$usersNew?>,
                'getmes' : <?=$messagesGet?>,
                'sendmes' : <?=$messagesSend?>,
                'reviews' : <?=$reviews?>,
                'orders' : <?=$orders?>,
            }
        </script>
        <div class="row">
            <div class="col s12 xl6">
                <div class="card statsCard hoverable">
                    <div class="card-title">Пользователи:</div>
                    <div class="chart">
                        <div id="chart-users"></div>
                    </div>
                </div>
            </div>
            <div class="col s12 xl6">
                <div class="card statsCard hoverable">
                    <div class="card-title">Новые пользователи:</div>
                    <div class="chart">
                        <div id="chart-newusers"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 xl6">
                <div class="card statsCard hoverable">
                    <div class="card-title">Сообщений получено:</div>
                    <div class="chart">
                        <div id="chart-getmes"></div>
                    </div>
                </div>
            </div>
            <div class="col s12 xl6">
                <div class="card statsCard hoverable">
                    <div class="card-title">Сообщений отправлено:</div>
                    <div class="chart">
                        <div id="chart-sendmes"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 xl6">
                <div class="card statsCard hoverable">
                    <div class="card-title">Количество отзывов:</div>
                    <div class="chart">
                        <div id="chart-reviews"></div>
                    </div>
                </div>
            </div>
            <div class="col s12 xl6">
                <div class="card statsCard hoverable">
                    <div class="card-title">Всего заказов:</div>
                    <div class="chart">
                        <div id="chart-orders"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>