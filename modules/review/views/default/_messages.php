<?
if (is_array($messages)) {
    foreach ($messages as $message) {
?>
    <div class="dialogMessage <?= $message['is_bot'] ? 'dialogMessage_right' : '' ?>">
        <div class="dialogMessage__date"><?= $message['date'] ?></div><?
        switch ($message['action']) {
            case 'sendSticker':
                ?>
                <div class="dialogMessage__block">
                    <img src="<?= $message['text'] ?>" alt="" style="max-width: 100%; height: 300px">
                </div>
                <?
                break;
            case 'pressButton':
                ?>
                <div class="dialogMessage__action">
                    Нажатие на кнопку <b><?= $message['text'] ?></b>
                </div>
                <?
                break;
            case 'answerCallbackQuery':
                ?>
                <div class="dialogMessage__action">
                    <b>Отправлено уведомление:</b><br/> "<?= $message['text'] ?>"
                </div>
                <?
                break;
            case 'apiError':
                ?>
                <div class="dialogMessage__error">
                    <b>Ошибка при отправке сообщения:</b><br/> "<?= $message['text'] ?>"
                </div>
                <?
                break;
            default:
                if(strlen($message['text'])){?>
                    <div class="dialogMessage__block">
                        <div class="dialogMessage__text"><?= $message['text'] ?></div>
                    </div>
                <?}
                break;
        }

        if($message['buttons']){
            switch ($platform){
                case 'telegram':
                    foreach($message['buttons'] as $button){
                        ?><div class="dialogMessage__btn" <?=$button['bg_color'] ? 'style="background-color: '.$button['bg_color'].'"' : ''?>><?=$button['text']?></div><?
                    }
                    break;
                case 'viber':
                    ?><div class="viberButtonsGroup"><?
                    foreach($message['buttons'] as $button){

                        $className = "dialogMessage__btn viberButton ";
                        $className .= $button['rows'] ? "viberButton_rows_".$button['rows'].' ' : '';
                        $className .= $button['columns'] ? "viberButton_cols_".$button['columns'].' ' : '';
                        $className .= $button['image'] ? "viberButton_image".' ' : '';

                        $style = 'style="';
                        $style .= $button['bg_color'] ? 'background-color:'.$button['bg_color'].'; ' : '';
                        $style .= $button['image'] ? 'background-image: url('.$button['image'].'); ' : '';
                        $style .= $button['text_h_align'] ? 'text-align: '.$button['text_h_align'].'; ' : '';
                        $style .= '"';

                        ?><div class="<?=$className?>" <?=$style?>><span><?=$button['text']?></span></div><?
                    }
                    ?></div><?
                    break;
                default:
                    break;
            }
        }
        ?>
    </div>
<?
    }
}
?>