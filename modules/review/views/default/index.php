<?
$this->title = 'Отзывы';
$userId = $reviews[$id]->startMessage->user_bot_id;
$platformName = $users[$userId]->platformName;
?>

<div class="col s6 xxl5 grey lighten-3 wrapper-container">
  <div class="section">
    <form action="" class="reviewForm">
      <div class="row m0">
        <div class="col s3">
          <div class="input-field m0">
            <select name="status">
              <option value="" disabled selected>Статус отзыва</option>
              <?
                foreach ($statuses as $key => $status) {
                  echo '<option  value="'.$key.'" '.((isset($_GET['status']) && $key==$_GET['status'])?'selected':'').'>'.$status.'</option>';
                }
              ?>
            </select>
          </div>
        </div>
        <div class="col s8">
          <div class="input-field m0">
            <input name="search" type="search" placeholder="Поиск" class="m0 p10" value="<?=$_GET['search']?>">
          </div>
        </div>
        <div class="col s1 flex-btn">
          <!-- <i class="material-icons small">volume_up</i> -->
          <i class="material-icons small reset">close</i>
        </div>
      </div>
    </form>
  </div>
  <div class="divider"></div>
  <div class="section wrapper-section section_review">
    <div class="wrapper users">
      <?= $this->render('_users', [
          'id' => $id,
          'users' => $users,
          'reviews' => $reviews,
          'statuses' => $statuses,
      ]) ?>
    </div>
  </div>
</div>
<div class="col s6 xxl7 grey lighten-4 wrapper-container">
  <?if ($userId) {?>
  <div class="section white z-depth-2">
    <div class="row m0">
      <div class="col s8 xxm10">
        <div class="users__img">
          <div class="img <?=$users[$userId]->image ? 'fz0' : ' no_image color'.(strlen($users[$userId]->name)%5) ?>" style="background-image: url(<?=$users[$userId]->image?>)">
              <? 
                $arName = explode(' ', $users[$userId]->name);
                foreach ($arName as $word){
                    echo(mb_substr($word, 0, 1));
                }
              ?>
          </div>
          <div class="img-label img-label-<?=$users[$userId]->platform?>"></div>
        </div>
        <div class="flow-text truncate"><?=$users[$userId]->name?></div>
        <div class="big-text"><?=($users[$userId]->phone?'Телефон: '.$users[$userId]->phone:'Телефон не указан')?></div>
      </div>
      <?if ($reviews[$id]->status != 2) {?>
        <a class="btn col s4 xxm2 mt14" href="/review/complete/<?=$id?>">Обработан</a>
      <?}?>
    </div>
  </div>
  <?}?>

  <div class="section wrapper-section wrapper-section_messages">
    <div class="wrapper messages">
      <?= $users[$userId] ?
        Yii::$app->view->renderFile('@app/modules/bot/views/default/_messages.php', [
          'platform' => $users[$userId]->platformName,
          'messages' => $users[$userId]->getReviewMessages($id),
      ]) : '' ?>
    </div>
  </div>

  <?if ($userId) {?>
  <div class="divider white"></div>
  <form class="ajaxForm userConnectBlock section white <?=(($users[$userId]->connect == 2 && $reviews[$id]->status != 2)?'connected':'')?>" action="/api/<?=$platformName?>/send_message/<?=$userId?>" method="post">
    <div class="row m0">
      <div class="col s6 xxm8">
        <div class="input-field m0">
          <!-- <input type="hidden" name="reviewId" value="<?//=$id?>"> -->
          <input type="text" placeholder="Введите сообщение" name="text" class="m0">
        </div>
      </div>
      <div class="col s6 xxm4 flex-btn">
        <!--<i class="material-icons small">person</i>
        <i class="material-icons small">close</i>
        <i class="material-icons small">block</i>
        <i class="material-icons small">mood</i>-->
        <!-- <button><i class="material-icons small">send</i></button> -->
        <i class="material-icons small sendBtn">send</i>
        <div class="btn userConnect" href="/api/<?=$platformName?>/enable/<?=$userId?>">Отключиться</div>
      </div>
    </div>
    <div class="connectBlock">
      <div class="btn userConnect <?=($reviews[$id]->status==2)?'disabled':''?>" href="/api/<?=$platformName?>/disable/<?=$userId?>/?review_id=<?=$id?>">Подключиться к диалогу</div>
    </div>
  </form>
  <?}?>
</div>

<script>
  global_reviewID = <?=$id?$id:'false'?>;
</script>
