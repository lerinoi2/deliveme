<?

if (count($reviews) > 0) {
  foreach ($reviews as $review) {
      $startMessage = $review->startMessage;
      $replyMessage = $review->replyMessage;
      $user = $users[$startMessage->user_bot_id];

      if ($user) {
?>
    <?= $this->render('_user', [
        'review' => $review,
        'startMessage' => $startMessage,
        'replyMessage' => $replyMessage,
        'user' => $user,
        'status' => $statuses[$review->status],
        'is_cur' => ($review->id == $id),
    ]) ?>
<?
    }
  }
} else {
  echo '<div class="flow-text center-align">Ничего не найдено</div>';
}
?>