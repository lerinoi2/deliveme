<div data-section="review" 
     data-id="<?=$review->id?>" 
     data-status="<?=$review->status?>"
     data-date="<?=strtotime($startMessage->date)?>"
     class="card-panel hoverable users__card bg-<?=$review->status?> link <?=$is_cur?'light-green accent-4':''?>" >
  <div class="row valign-wrapper m0">
    <div class="col s6">
      <div class="users__img">
        <div class="img <?=$user->image ? 'fz0' : ' no_image color'.(strlen($user->name)%5) ?>" style="background-image: url(<?=$user->image?>)">
            <? 
              $arName = explode(' ', $user->name);
              foreach ($arName as $word){
                  echo(mb_substr($word, 0, 1));
              }
            ?>
        </div>
        <div class="img-label img-label-<?=$user->platform?>"></div>
      </div>
      <div class="big-text truncate"><?=$user->name?></div>
      <div class="middle-text"><?=$startMessage->datestr?></div>
      <div class="small-text">Дата и время создания</div>
    </div>
    <div class="col s3 center-align borders">
      <div class="big-text"><?=$status?></div>
      <!-- <div class="middle-text"><?='Оператор: '.$review->operator_id?></div> -->
      <div class="middle-text"><?=($replyMessage?$replyMessage->datestr:'Нет ответа')?></div>
      <div class="small-text">Дата и время ответа</div>
    </div>
    <div class="col s3 center-align">
      <div class="big-text">
        <?for ($i=0; $i < 5; $i++) { 
          echo '<i class="material-icons">';
          if ($i < $review->mark) {
            echo 'star';
          } else {
            echo 'star_border';
          }
          echo '</i>';
        }?>
      </div>
      <!-- <div class="middle-text"><?='Заказ: '.$review->order_id?></div>
      <div class="small-text"><?=$startMessage->datestr?></div> -->
    </div>
  </div>
</div>