<?php

namespace app\modules\review\controllers;

use Yii;
use yii\web\Controller;
use app\modules\bot\models\Bot;
use app\modules\bot\models\UserBot;
use app\modules\bot\models\Message;
use app\modules\review\models\Review;

/**
 * Default controller for the `review` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
            return false;
        }
        return true;
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id)
    {
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
        $queryUsers = UserBot::find()->where(['bot_id' => $bot_id])->indexBy('id');
        $query = Review::find()->indexBy('id')->orderBy('status, id DESC');
        $resUsers = array();
        $resReviews = array();
        $search = Yii::$app->request->get('search');
        $status = Yii::$app->request->get('status');
        if ($status != null) {
            $query->andWhere(['status' => $status]);
        }
        if ($search != null) {
            $queryUsersCopy = clone $queryUsers;
            $users = $queryUsersCopy->andWhere(['like', 'name', $search])->select('id')->all();

            $messages = Message::find()->where(['and',['not',['review_id' => null]],['or', ['like', 'text', $search], ['user_bot_id' => array_keys($users)]]])->all();
            foreach ($messages as $message) {
                $resUsers[] = $message->user_bot_id;
                $resReviews[] = $message->review_id;
            }

            $queryUsers->andWhere(['or', ['like', 'name', $search], ['id' => $resUsers]]);
            $query->andWhere(['id' => $resReviews]);
        }

        if ($id) {
            Review::updateAll(['status' => Review::STATUS['PROCESSING']], ['id' => $id, 'status' => Review::STATUS['NEW']]);
            Message::updateAll(['is_read' => 1], ['review_id' => $id, 'is_read' => 0]);
        }

        $users = $queryUsers->all();
        $mess_table = Message::tableName();
        $rows = (new \yii\db\Query())
            ->select([$mess_table.'.review_id'])
            ->from($mess_table)
            ->where(['user_bot_id' => array_keys($users)])
            ->andWhere(['not', ['review_id' => null]])
            ->indexBy('review_id')
            ->all();
        $reviews = $query->andWhere(['id' => array_keys($rows)])->all();
        $statuses = Review::getStatusArray();
        return $this->render('index',['users' => $users, 'statuses' => $statuses, 'reviews' => $reviews, 'id' => $id]);
    }

    public function actionComplete($id)
    {
        $review = Review::findOne($id);
        $review->status = Review::STATUS['COMPLETED'];
        $review->save(false);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUser($id)
    {
        $review = Review::findOne($id);
        $startMessage = $review->startMessage;
        $replyMessage = $review->replyMessage;
        $user = UserBot::findOne($startMessage->user_bot_id);
        return $this->renderPartial('_user', [
            'review' => $review,
            'startMessage' => $startMessage,
            'replyMessage' => $replyMessage,
            'user' => $user,
            'status' => Review::getStatusArray()[$review->status],
            'is_cur' => ($review->id == Yii::$app->request->get('review_id')),
        ]);
    }
}
