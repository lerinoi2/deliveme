<?php

namespace app\modules\review\models;

use Yii;
use app\modules\bot\models\Message;
use app\modules\bot\models\UserBot;
use app\modules\webhooks\models\ViberApi;
use app\modules\webhooks\models\TelegramApi;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property int $id
 * @property int $status
 * @property int $order_id
 * @property int $mark
 * @property int $operator_id
 *
 * @property Messages[] $messages
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'order_id', 'mark', 'operator_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'order_id' => 'Order ID',
            'mark' => 'Mark',
            'operator_id' => 'Operator ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['review_id' => 'id'])->orderBy('date')->all();
    }

    public function getStartMessage()
    {
        return $this->hasMany(Message::className(), ['review_id' => 'id'])->orderBy('date')->one();
    }

    public function getReplyMessage()
    {
        return $this->hasMany(Message::className(), ['review_id' => 'id'])
            ->where(['is_bot' => true])
            ->orderBy('date DESC')
            ->one();
    }

    public function getUserBot()
    {
        $mess_table = Message::tableName();
        $user_table = UserBot::tableName();
        return UserBot::find()
            ->leftJoin($mess_table, $mess_table.'.user_bot_id = '.$user_table.'.id')
            ->where([$mess_table.'.review_id' => $this->id])
            ->one();
    }

    public static function getByUserID($user_id)
    {
        // Переписать !!! 
        $messages = Message::find()->where(['user_bot_id' => $user_id])->andWhere(['not', ['review_id' => null]])->all();

        $review = false;
        if($messages){
            foreach ($messages as $message){
                if(!$review){
                    $review = self::find()->where(['id' => $message->review_id, 'complete' => 0])->one();
                }
            }
        }

        if(!$review){
            $review = new self();
            $review->status = 0;
            $review->operator_id = 1;
            $review->complete = 0;
            $review->save();
        }

        return $review;
    }

    public function complete()
    {
        if($this->complete) return;
        
        $this->complete = true;
        $this->save();

        if (($this->userBot->bot_id == 2) && ($this->mark > 0) && ($this->mark < 5)) {
            // $this->sendReviewAlert(['tVudWHcSKC21QsuIrYq6rA==', 'vPm5unEnRj+FF6FvUMZ1NA==']); // for test
            $this->sendReviewAlert(['k732+I9y4pXGWRAsz9Nr6w==', 'QPPWrtWpmqrOiEcu/sXy3g==', '08V1L+hu5Z8XYBdTUZc/Gw==', 'phfXIsZ557rVnWDyZLQxQQ==']);
        }
    }

    public function toText()
    {
        $messages = $this->messages;
        $user = $this->userBot;
        $platforms = [
            0 => 'Telegram',
            1 => 'Viber'
        ];

        $text = "Новый отзыв (".$platforms[$user->platform].")\n";
        $text .= "Пользователь: ".$user->name;
        if($this->mark){
            $text .= "\nОценка: ";
            for($i = 0; $i < $this->mark; $i++){
                $text .= "⭐️";
            }
        }
        $text .= "\nТекст:\n";

        foreach ($messages as $message)
        {
            if($message->action === 'message' && !$message->is_bot){
                $text .= $message->text."\n";
            }
        }

        return $text;
    }

    public function sendReviewAlert($receivers)
    {
        $text = $this->toText();

        foreach ($receivers as $receiver) {
            $user = UserBot::find()->where(['chat_id' => $receiver])->select('id, platform')->one();
            if ($user) {
                if ($user->platform == 0) {
                    $api = new TelegramApi($user->id);
                } else if ($user->platform == 1) {
                    $api = new ViberApi($user->id);
                }
                $api->sendMessage($text);
            }
        }
    }

    const STATUS = [
        'NEW' => 0,
        'PROCESSING' => 1,
        'COMPLETED' => 2,
    ];
    public static function getStatusArray()
    {
        return array(
            self::STATUS['NEW'] => 'Новый',
            self::STATUS['PROCESSING'] => 'В работе',
            self::STATUS['COMPLETED'] => 'Завершён',
        );
    }
}
