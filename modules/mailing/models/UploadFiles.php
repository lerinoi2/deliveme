<?
namespace app\modules\mailing\models;

use Yii;
use yii\base\Model;
use yii\helpers\BaseFileHelper;

class UploadFiles extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 4],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $res = array();
            foreach ($this->imageFiles as $file) {
                $photopath = new BaseFileHelper();
                $photofilepath = '/upload/'.Yii::$app->getSecurity()->generateRandomString(2);
                $photopath->createDirectory(Yii::getAlias('@webroot').$photofilepath);
                $photofilename = Yii::$app->getSecurity()->generateRandomString(12). '.' . $file->extension;

                $path = Yii::getAlias('@webroot').$photofilepath . '/' . $photofilename;
                if( $file->saveAs($path) ) { 
                    $res[] = [
                        'n' => $file->name,
                        'p' => $path
                    ];
                }
            }
            return json_encode($res);
        } else {
            return null;
        }
    }
}