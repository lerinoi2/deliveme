<?php

namespace app\modules\mailing\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\mailing\models\Book;
use app\modules\bot\models\Bot;

/**
 * BookSearch represents the model behind the search form of `app\modules\mailing\models\Book`.
 */
class BookSearch extends Book
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'count'], 'integer'],
            [['name', 'date', 'users', 'period', 'orders', 'reviews', 'menu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
        $query = Book::find()->where(['bot_id' => $bot_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'count' => $this->count,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'users', $this->users])
            ->andFilterWhere(['like', 'period', $this->period])
            ->andFilterWhere(['like', 'orders', $this->orders])
            ->andFilterWhere(['like', 'reviews', $this->reviews])
            ->andFilterWhere(['like', 'menu', $this->menu]);

        return $dataProvider;
    }
}
