<?php

namespace app\modules\mailing\models;
use app\modules\bot\models\Bot;

use Yii;

/**
 * This is the model class for table "{{%mails}}".
 *
 * @property int $id
 * @property string $name
 * @property string $date_create
 * @property int $status
 * @property string $date_send
 * @property int $template_id
 * @property int $book_id
 * @property string $plan
 * @property string $text
 * @property string $files
 * @property int $postpone_date
 * @property int $bot_id
 *
 * @property Books $book
 * @property Bots $bot
 * @property Templates $template
 */
class Mail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mails}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'bot_id'], 'required', 'message' => '{attribute} не может быть пустым.'],
            [['date_create', 'date_send'], 'safe'],
            [['status', 'template_id', 'book_id', 'bot_id'], 'integer'],
            [['text'], 'string'],
            [['name', 'files', 'plan'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Book::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => Template::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название рассылки',
            'date_create' => 'Дата и время',
            'status' => 'Статус',
            'date_send' => 'Дата отправки',
            'template_id' => 'Шаблон',
            'book_id' => 'База',
            'text' => 'Текст сообщения',
            'files' => 'Файлы',
            'bot_id' => 'Bot ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'book_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }

    const STATUS = [
        'WAITING' => 0,
        'SENT' => 1,
    ];
    public static function getStatusArray()
    {
        return array(
            self::STATUS['WAITING'] => 'Ожидает',
            self::STATUS['SENT'] => 'Отправлено',
        );
    }
}
