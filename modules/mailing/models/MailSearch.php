<?php

namespace app\modules\mailing\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\mailing\models\Mail;
use app\modules\bot\models\Bot;

/**
 * MailSearch represents the model behind the search form of `app\modules\mailing\models\Mail`.
 */
class MailSearch extends Mail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'template_id', 'book_id'], 'integer'],
            [['name', 'date_create', 'date_send'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
        $query = Mail::find()->where(['bot_id' => $bot_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_create' => $this->date_create,
            'status' => $this->status,
            'date_send' => $this->date_send,
            'template_id' => $this->template_id,
            'book_id' => $this->book_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
