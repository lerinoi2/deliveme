<?php

namespace app\modules\mailing\models;
use app\modules\bot\models\Bot;
use app\modules\bot\models\UserBot;

use Yii;

/**
 * This is the model class for table "{{%books}}".
 *
 * @property int $id
 * @property string $name
 * @property string $date
 * @property string $users
 * @property string $period
 * @property string $orders
 * @property string $reviews
 * @property string $menu
 * @property int $count
 * @property int $bot_id
 *
 * @property Bots $bot
 * @property Mails[] $mails
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%books}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'bot_id'], 'required', 'message' => '{attribute} не может быть пустым.'],
            [['date'], 'safe'],
            [['users'], 'string'],
            [['count', 'bot_id'], 'integer'],
            [['name', 'period', 'orders', 'reviews', 'menu'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date' => 'Дата создания',
            'users' => 'Users',
            'period' => 'Период заказов',
            'orders' => 'Заказы',
            'reviews' => 'Отзывы',
            'menu' => 'Menu',
            'count' => 'Количество',
            'bot_id' => 'Bot ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMails()
    {
        return $this->hasMany(Mail::className(), ['book_id' => 'id']);
    }

    public function getSelect()
    {
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
        $res = self::find()->where(['bot_id' => $bot_id])->select(['id','name'])->all();
        foreach ($res as $item) {
            $return[$item->id] = $item->name;
        }
        return $return?$return:Array();
    }

    public function getCountUsers()
    {
        return count(json_decode($this->users));
    }

    public function getCountOrders()
    {
        $users = UserBot::find()->where(['id' => json_decode($this->users)])->select('orders')->all();
        $count = 0;
        foreach ($users as $user) {
            $count += $user->orders;
        }
        return $count;
    }
}
