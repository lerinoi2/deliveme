<?php

namespace app\modules\mailing\models;
use app\modules\bot\models\Bot;

use Yii;

/**
 * This is the model class for table "{{%templates}}".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $files
 * @property string $date
 * @property int $bot_id
 *
 * @property Mails[] $mails
 * @property Bots $bot
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%templates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text', 'bot_id'], 'required', 'message' => '{attribute} не может быть пустым.'],
            [['date'], 'safe'],
            [['text'], 'string'],
            [['bot_id'], 'integer'],
            [['name', 'files'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['bot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bot::className(), 'targetAttribute' => ['bot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название шаблона',
            'text' => 'Текст сообщения',
            'files' => 'Вложения',
            'date' => 'Дата создания',
            'bot_id' => 'Bot ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMails()
    {
        return $this->hasMany(Mail::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBot()
    {
        return $this->hasOne(Bot::className(), ['id' => 'bot_id']);
    }

    public function getSelect()
    {
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
        $res = self::find()->where(['bot_id' => $bot_id])->select(['id','name'])->all();
        foreach ($res as $item) {
            $return[$item->id] = $item->name;
        }
        return $return?$return:Array();
    }
}
