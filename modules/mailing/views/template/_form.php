<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\mailing\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="side-form side-form_temp">

    <h5 class="center-align">Создание шаблона сообщения</h5>

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'action' => '/mailing/templates/create'
    ]); ?>

    <?= $form->field($model, 'name', ['options'=>['class'=>'input-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text', ['options'=>['class'=>'input-field templateText']])->textarea([
        'class'=>'materialize-textarea tooltipped', 
        'maxlength' => 300, 
        'data-length'=>300, 
        'data-position' => 'right', 
        'data-tooltip' => "Данные пользователя:<br>*name* - Имя", 
    ]) ?>

    <?= $form->field($upload, 'imageFiles[]', ['template' => '
      <div class="btn">
        <span><i class="material-icons">attach_file</i></span>
        {input}
      </div>
      <div class="file-path-wrapper">
        <input class="file-path" type="text" placeholder="Прикрепить">
      </div>',
      'options'=>['class'=>'file-field input-field col s6 templateFiles']
    ])->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn col s4 offset-s2 mt24']) ?>

    <?php ActiveForm::end(); ?>

</div>
