<?
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Рассылки';
$subtitle = 'Шаблоны сообщений';
?>
<nav class="grey lighten-2">
  <div class="nav-wrapper">
    <span class="title grey-text text-darken-3"><?=$subtitle?></span>
    <ul class="right hide-on-med-and-down">
      <li><a href="/mailing/create" class="waves-effect waves-light btn grey">Создать рассылку</a></li>
      <li><a href="/mailing" class="waves-effect waves-light btn grey">История рассылок</a></li>
      <li><a href="/mailing/books" class="waves-effect waves-light btn grey">Адресные книги</a></li>
      <li><a href="/mailing/templates" class="waves-effect waves-light btn ">Шаблоны сообщений</a></li>
    </ul>
  </div>
</nav>

<div class="col s4 white ">
<?= $this->render('_form', [
    'model' => $model,
    'upload' => $upload,
]) ?>
</div>

<div class="col s8 grey lighten-4 table-wrapper">

<?
$columns = [
    'name',
    [
        'attribute' => 'date',
        'format' => ['date', 'php:d.m.Y H:i'],
        'filterOptions' => [
            'class' => 'date-field'
        ],
    ], 
    [
        'attribute' => 'files',
        'format' => 'html',
        'value' => function ($data){
            $str = '';
            $arr = json_decode($data['files']);
            foreach ($arr as $item) {
              $str .= '<i class="material-icons" title="'.$item->n.'">attach_file</i>';
            }
            return $str;
        },
    ], 
    [
        'attribute' => 'text',
        'headerOptions' => ['style' => 'width:400px;'],
        'format' => 'html',
        'value' => function ($data){
            return '<span class="truncate">'.$data['text'].'</span>';
        },
    ], 
    // 'text:ntext',
];
?>

<?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'layout'=>"{items}\n{pager}",
    'tableOptions' => [
        'class' => 'highlight templatesTable'
    ],
    'emptyText' => 'Нет шаблонов',
    'pager' => [
        'pageCssClass' => 'waves-effect',
        'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
        'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
        // 'activePageCssClass' => 'def-bg',
        'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'disabled']
    ],
    'columns' => $columns,
]); ?>
<?php Pjax::end(); ?>
</div>
