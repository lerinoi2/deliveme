<?
$this->title = 'Рассылки';
$subtitle = 'Создать рассылку';
?>
<nav class="grey lighten-2">
  <div class="nav-wrapper">
    <span class="title grey-text text-darken-3"><?=$subtitle?></span>
    <ul class="right hide-on-med-and-down">
      <li><a href="/mailing/create" class="waves-effect waves-light btn ">Создать рассылку</a></li>
      <li><a href="/mailing" class="waves-effect waves-light btn grey">История рассылок</a></li>
      <li><a href="/mailing/books" class="waves-effect waves-light btn grey">Адресные книги</a></li>
      <li><a href="/mailing/templates" class="waves-effect waves-light btn grey">Шаблоны сообщений</a></li>
    </ul>
  </div>
</nav>

<div class="col s12 grey lighten-4 table-wrapper ov-auto">
  <?= $this->render('_form', [
      'model' => $model,
      'upload' => $upload,
      'booksSelect' => $booksSelect,
      'templatesSelect' => $templatesSelect,
  ]) ?>
</div>
