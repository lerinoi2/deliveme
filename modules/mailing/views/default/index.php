<?
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Рассылки';
$subtitle = 'История рассылок';
?>
<nav class="grey lighten-2">
  <div class="nav-wrapper">
    <span class="title grey-text text-darken-3"><?=$subtitle?></span>
    <ul class="right hide-on-med-and-down">
      <li><a href="/mailing/create" class="waves-effect waves-light btn grey">Создать рассылку</a></li>
      <li><a href="/mailing" class="waves-effect waves-light btn ">История рассылок</a></li>
      <li><a href="/mailing/books" class="waves-effect waves-light btn grey">Адресные книги</a></li>
      <li><a href="/mailing/templates" class="waves-effect waves-light btn grey">Шаблоны сообщений</a></li>
    </ul>
  </div>
</nav>

<div class="col s12 grey lighten-4 table-wrapper">

<?
$countUsers = 
$columns = [
    'name',
    [
        'attribute' => 'date_create',
        'format' => ['date', 'php:d.m.Y H:i'],
        'filterOptions' => [
            'class' => 'date-field'
        ],
    ], 
    [
        'attribute' => 'status',
        'value' => function ($data) use($statusArray){
            return $statusArray[$data['status']];
        },
        'filter'=> $statusArray,
        'filterOptions' => [
            'class' => 'input-field'
        ],
    ], 
    [
        'attribute' => 'date_send',
        'format' => ['date', 'php:d.m.Y H:i'],
        'filterOptions' => [
            'class' => 'date-field'
        ],
    ], 
    [
        'attribute' => 'template_id',
        'value' => function ($data) use($statusArray){
            return $data->template->name;
        },
    ], 
    [
        'attribute' => 'book_id',
        'value' => function ($data) use($statusArray){
            return $data->book->name;
        },
    ], 
    // 'template_id',
    // 'book_id',
    [
        'label' => 'Всего',
        'value' => function ($data) {
            return $data->book->countUsers;
        },
    ], 
    [
        'label' => 'Доставлено',
        'value' => function ($data) {
            return $data['status']?$data->book->countUsers:0;
        },
    ], 
    [
        'label' => 'Прочитано',
        'value' => function ($data) {
            return $data['status']?$data->book->countUsers:0;
        },
    ], 
    [
        'label' => 'Заказов',
        'value' => function ($data) {
            return $data->book->countOrders;
        },
    ], 
];
?>


<?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-'],
    'layout'=>"{items}\n{pager}",
    'tableOptions' => [
        'class' => 'highlight centered'
    ],
    'emptyText' => 'Нет рассылок',
    'pager' => [
        'pageCssClass' => 'waves-effect',
        'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
        'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
        // 'activePageCssClass' => 'def-bg',
        'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'disabled']
    ],
    'columns' => $columns,
]); ?>
<?php Pjax::end(); ?>

</div>
