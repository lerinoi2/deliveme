<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="mail-form white">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name', ['options'=>['class'=>'input-field col s12']])->textInput(['maxlength' => true]) ?>
    <div class="clear"></div>

    <?= $form->field($model, 'book_id', ['options'=>['class'=>'input-field col s8']])->dropDownList($booksSelect, ['prompt' => 'Выберите базу'])->label('') ?>
    <a href="/mailing/books" class="btn col s4 mt24">Создать базу</a>
    <div class="clear"></div>

    <?= $form->field($model, 'template_id', ['options'=>['class'=>'input-field col s8 templateId']])->dropDownList($templatesSelect, ['prompt' => 'Выберите шаблон'])->label('') ?>
    <a href="/mailing/templates" class="btn col s4 mt24">Создать шаблон</a>
    <!-- <div class="btn col s4 mt24 selectTemplate">Создать шаблон</div> -->
    <div class="clear"></div>

    <?= $form->field($model, 'text', ['options'=>['class'=>'input-field col s12 templateText']])
        ->textarea([
            'class'=>'materialize-textarea tooltipped', 
            'maxlength' => 300, 
            'data-length'=>300, 
            'data-position' => 'right', 
            'data-tooltip' => "Данные пользователя:<br>*name* - Имя", 
        ])
    ?>
    <div class="clear"></div>

    <div class="col s6 mt24">
        <input type="checkbox" id="save_as_temp" name="save_as_temp" class="filled-in">
        <label for="save_as_temp">Сохранить как шаблон</label>
    </div>
    <div class="input-field col s6">
        <label for="temp_name">Название шаблона</label>
        <input id="temp_name" name="temp_name" type="text">
    </div>
    <div class="clear"></div>

    <div class="col s2 bold mt14">Отложить</div> 
    <div class="switch col s2 mt14">
      <label>
        <input type="checkbox" name="Postpone[check]">
        <span class="lever"></span>
      </label>
    </div>
    <div class="col s4">
        <div class="datetime-field">
            <input type="text" name="Postpone[date]" placeholder="Дата и время">
        </div>
    </div>
    <div class="clear"></div>

    <div class="col s2 bold mt14">Повторять</div> 
    <div class="switch col s2 mt14">
      <label>
        <input type="checkbox" name="Repeat[check]" disabled>
        <span class="lever"></span>
      </label>
    </div>
    <div class="col s4">
        <select name="Repeat[type]" disabled>
          <option value="1" selected>Ежедневно</option>
          <option value="2">Еженедельно</option>
          <option value="3">Ежемесячно</option>
        </select>
    </div>
    <div class="col s2">
        <div class="time-field">
            <input type="text" name="Repeat[time]" placeholder="Время" disabled>
        </div>
    </div>
    <div class="clear"></div>

    <?= $form->field($upload, 'imageFiles[]', ['template' => '
      <div class="btn">
        <span><i class="material-icons">attach_file</i></span>
        {input}
      </div>
      <div class="file-path-wrapper">
        <input class="file-path" type="text" placeholder="Прикрепить">
      </div>',
      'options'=>['class'=>'file-field input-field col s6 templateFiles']
    ])->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?= Html::submitButton('Отправить', ['class' => 'btn col s4 offset-s2 mt24 mail-submit']) ?>
    <div class="clear"></div>

    <?php ActiveForm::end(); ?>

    <div class="preloader-container">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div><div class="gap-patch">
              <div class="circle"></div>
            </div><div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
    </div>
</div>
