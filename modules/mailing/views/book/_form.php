<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\mailing\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="side-form">

    <h5 class="center-align">Создание адресной книги</h5>

    <?php $form = ActiveForm::begin([
        'action' => '/mailing/books/create',
        'options' => ['class' => 'booksForm']
        
    ]); ?>

    <?= $form->field($model, 'name', ['options'=>['class'=>'input-field col s12']])->textInput(['maxlength' => true]) ?>
    
    <div class="switch col s3">
      <label>
        <span class="black-text">Telegram</span>
        <input type="checkbox" name="Platforms[telegram]">
        <span class="lever"></span>
      </label>
    </div>
    <div class="switch col s3">
      <label>
        <span class="black-text">Viber</span>
        <input type="checkbox" name="Platforms[viber]">
        <span class="lever"></span>
      </label>
    </div>
    <!-- <div class="switch col s3">
      <label>
        <span class="black-text">Vk</span>
        <input type="checkbox" name="Platforms[vk]">
        <span class="lever"></span>
      </label>
    </div>
    <div class="switch col s3">
      <label>
        <span class="black-text">Facebook</span>
        <input type="checkbox" name="Platforms[facebook]">
        <span class="lever"></span>
      </label>
    </div> -->
    <div class="clear"></div>

    <div class="col s5 mt24">
        <span>Заказывали в период с</span>
    </div>
    <div class="col s3">
        <div class="date-field">
            <input type="text" name="Period[start]" placeholder="Дата" disabled>
        </div>
    </div>
    <div class="col s1">
        <div class="mt14">по</div>
    </div>
    <div class="col s3">
        <div class="date-field">
            <input type="text" name="Period[end]" placeholder="Дата" disabled>
        </div>
    </div>
    <div class="clear"></div>

    <div class="col s6 mt24">
        <span>Количество заказов за период, с</span>
    </div>
    <div class="col s2">
        <select name="Orders[start]" disabled>
            <?for ($i=1; $i <= 12; $i++) {
                echo "<option value=".$i." ".($i==1?'selected':'').">".$i."</option>";
            } ?>
        </select>
    </div>
    <div class="col s1">
        <div class="mt14">по</div>
    </div>
    <div class="col s2">
        <select name="Orders[end]" disabled>
            <?for ($i=1; $i <= 12; $i++) {
                echo "<option value=".$i." ".($i==1?'selected':'').">".$i."</option>";
            } ?>
        </select>
    </div>
    <div class="clear"></div>

    <div class="col s6 mt24">
        <span>Оставили отзывов за период, с</span>
    </div>
    <div class="col s2">
        <select name="Reviews[start]" disabled>
            <?for ($i=1; $i <= 12; $i++) {
                echo "<option value=".$i." ".($i==1?'selected':'').">".$i."</option>";
            } ?>
        </select>
    </div>
    <div class="col s1">
        <div class="mt14">по</div>
    </div>
    <div class="col s2">
        <select name="Reviews[end]" disabled>
            <?for ($i=1; $i <= 12; $i++) {
                echo "<option value=".$i." ".($i==1?'selected':'').">".$i."</option>";
            } ?>
        </select>
    </div>
    <div class="clear"></div>

    <div class="col s6 mt7">
        <span>Заказывали из меню за период:</span>
    </div>
    <div class="col s5 center-align">
        <span class="btn disabled">Выбрать</span>
    </div>
    <div class="clear"></div>
    <div class="col s12 book-chips">
        <!-- <div class="chip">
            Tag1
            <i class="close material-icons">close</i>
        </div>
        <div class="chip">
            Tag2
            <i class="close material-icons">close</i>
        </div> -->
    </div>
    <div class="clear"></div>

    <div class="divider"></div>
    <div class="book-count center-align">
        <div>Количество получателей в адресной книге:</div>
        <span class="counter">0</span>
    </div>
    <div class="divider"></div>

    <input type="hidden" name="users" class="usersInput">
    <div class="center-align">
        <?= Html::submitButton('Создать', ['class' => 'btn mt24']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
