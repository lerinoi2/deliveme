<?
// use Yii;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Рассылки';
$subtitle = 'Адресные книги';
?>
<nav class="grey lighten-2">
  <div class="nav-wrapper">
    <span class="title grey-text text-darken-3"><?=$subtitle?></span>
    <ul class="right hide-on-med-and-down">
      <li><a href="/mailing/create" class="waves-effect waves-light btn grey">Создать рассылку</a></li>
      <li><a href="/mailing" class="waves-effect waves-light btn grey">История рассылок</a></li>
      <li><a href="/mailing/books" class="waves-effect waves-light btn ">Адресные книги</a></li>
      <li><a href="/mailing/templates" class="waves-effect waves-light btn grey">Шаблоны сообщений</a></li>
    </ul>
  </div>
</nav>

<div class="col s4 white  table-wrapper ov-auto">
<?= $this->render('_form', [
    'model' => $model,
]) ?>
</div>

<div class="col s8 grey lighten-4 table-wrapper">

<?
$columns = [
    'name',
    [
        'attribute' => 'date',
        'format' => ['date', 'php:d.m.Y H:i'],
    ], 
    [
        'attribute' => 'count',
        'value' => function ($data) {
            return count(json_decode($data['users']));
        },
    ], 
    [
        'attribute' => 'period',
        'value' => function ($data) {
            $period = json_decode($data['period']);
            return $period[0].' - '.$period[1];
        },
    ], 
    [
        'attribute' => 'orders',
        'value' => function ($data) {
            $orders = json_decode($data['orders']);
            return $orders[0].' - '.$orders[1];
        },
    ], 
    [
        'attribute' => 'reviews',
        'value' => function ($data) {
            $reviews = json_decode($data['reviews']);
            return $reviews[0].' - '.$reviews[1];
        },
    ], 
];
?>

<?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-'],
    'layout'=>"{items}\n{pager}",
    'tableOptions' => [
        'class' => 'highlight centered'
    ],
    'emptyText' => 'Нет адресных книг',
    'pager' => [
        'pageCssClass' => 'waves-effect',
        'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
        'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
        // 'activePageCssClass' => 'def-bg',
        'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'disabled']
    ],
    'columns' => $columns
]); ?>
<?php Pjax::end(); ?>
</div>
