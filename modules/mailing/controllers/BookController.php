<?php

namespace app\modules\mailing\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use app\modules\bot\models\Bot;
use app\modules\mailing\models\Book;
use app\modules\mailing\models\BookSearch;
use app\modules\bot\models\UserBot;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
            return false;
        }
        return true;
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 14];

        $model = new Book();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();

        $params = Yii::$app->request->post();
        if ($params && $params['users'] != '[]') {
            $model->name = $params['Book']['name'];
            $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
            $model->bot_id = $bot_id; 
            $model->date = Yii::$app->formatter->asDatetime(time(), "php:Y-m-d H:i:s");
            $model->users = $params['users'];
            $model->platforms = json_encode(array_keys($params['Platforms']));
            $model->period = json_encode([$params['Period']['start'],$params['Period']['end']]);
            $model->orders = json_encode([$params['Orders']['start'],$params['Orders']['end']]);
            $model->reviews = json_encode([$params['Reviews']['start'],$params['Reviews']['end']]);
            $model->count = 0;

            $model->save();
        }

        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionUsers()
    {
        $res = array();
        $params = Yii::$app->request->post();
        
        $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
        $users = UserBot::find()->where(['bot_id' => $bot_id])->indexBy('id')->orderBy('id');

        $pFilter = array();
        if (is_array($params['Platforms'])) {
            foreach ($params['Platforms'] as $key => $value) {
                $pFilter[] = UserBot::PLATFORM[$key];
            }
        }
        $users->andWhere(['platform' => $pFilter]); // fix

        $res = array_keys($users->all());

        return json_encode($res);
    }
}
