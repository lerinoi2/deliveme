<?php

namespace app\modules\mailing\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use app\modules\bot\models\Bot;
use app\modules\bot\models\UserBot;
use app\modules\mailing\models\Mail;
use app\modules\mailing\models\Book;
use app\modules\mailing\models\Template;
use app\modules\mailing\models\BookSearch;
use app\modules\mailing\models\MailSearch;
use app\modules\mailing\models\UploadFiles;

use app\modules\webhooks\models\TelegramApi;
use app\modules\webhooks\models\ViberApi;

/**
 * Default controller for the `mailing` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest && $action->id != 'send') {
            $this->redirect('/');
            return false;
        }
        return true;
    }
    
    public function actionIndex()
    {
        $searchModel = new MailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 20];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statusArray' => Mail::getStatusArray(),
        ]);
    }

    public function actionCreate()
    {
        $model = new Mail();
        $upload = new UploadFiles();

        $params = Yii::$app->request->post();
        if ($params) {
            $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
            $model->bot_id = $bot_id;
            $model->name = $params['Mail']['name'];
            $model->date_create = Yii::$app->formatter->asDatetime(time(), "php:Y-m-d H:i:s");
            $model->status = 0;
            $model->book_id = $params['Mail']['book_id'];

            if ($params['Mail']['template_id']) {
                $model->template_id = $params['Mail']['template_id'];
            } else {
                $upload->imageFiles = UploadedFile::getInstances($upload, 'imageFiles');
                $files = $upload->upload();

                if ($params['save_as_temp']) {
                    $template = new Template();
                    $template->name = $params['temp_name'];
                    $template->text = $params['Mail']['text'];
                    $template->date = Yii::$app->formatter->asDatetime(time(), "php:Y-m-d H:i:s");
                    $template->files = $files;
                    $bot_id = Bot::find()->where(['user_id' => Yii::$app->user->id])->one()->id;
                    $template->bot_id = $bot_id;
                    $template->save();
                    $model->template_id = $template->id;
                } else {
                    $model->text = $params['Mail']['text'];
                    $model->files = $files;
                }
            }

            if ($params['Postpone']['check'] == 'on' && $params['Postpone']['date']) {
                $model->postpone_date = strtotime($params['Postpone']['date']);
            }
            if ($params['Repeat']['check'] == 'on' && $params['Repeat']['time']) {
                $plan = [
                    'type' => $params['Repeat']['type'],
                    'time' => $params['Repeat']['time']
                ];
            }
            if (isset($plan)) {
                $model->plan = json_encode($plan);
            }
            $model->save();
            if ($model->postpone_date) {
                return $this->redirect(['index']);
            } else {
                return $this->redirect(['send', 'id' => $model->id]);
            }
        }


        return $this->render('create', [
            'model' => $model,
            'upload' => $upload,
            'booksSelect' => Book::getSelect(),
            'templatesSelect' => Template::getSelect(),
        ]);
    }

    // */10 * * * * wget -O /dev/null http://lk.delive.me/mailing/send
    public function actionSend($id)
    {
        // TODO - идентифицировать пользователя
        $endTime = time() + 20;
        if ($id) {
            $mails = array(Mail::findOne($id));
        } else {
            $res = array();

            $mails = Mail::find()
                ->where(['status' => Mail::STATUS['WAITING']])
                ->andWhere(['<=','postpone_date', time()])
                ->orderBy('id')
                ->all();
        }

        $lastMailId = Yii::$app->request->get('lastMailId');
        $lastMailId = $lastMailId?$lastMailId:0;

        $lastUserId = Yii::$app->request->get('lastUserId');
        $lastUserId = $lastUserId?$lastUserId:0;

        $countMails = count($mails);
        for ($i = $lastMailId; $i < $countMails; $i++) { 

            if ($mails[$i]->template_id) {
                $template = $mails[$i]->template;
            } else {
                $template = [
                    'text' => $mails[$i]->text,
                    'files' => $mails[$i]->files,
                ];
            }

            $users = json_decode($mails[$i]->book->users);
            $countUsers = count($users);

            for ($j = $lastUserId; $j < $countUsers; $j++) {
                if (time() > $endTime) {
                    return $this->redirect(['send', 'id'=>$id, 'lastMailId'=>$i, 'lastUserId'=>$j], 301);
                }

                $user = UserBot::findOne($users[$j]);

                // Placeholders
                $text = str_replace('*name*', $user->name, $template['text']);

                if ($user->platform == UserBot::PLATFORM['telegram']) {
                    $telegramApi = new TelegramApi($users[$j]);
                    $telegramApi->sendMessage($text);
                    foreach (json_decode($template['files']) as $file) {
                        $telegramApi->sendPhoto($file->p);
                    }
                } elseif ($user->platform == UserBot::PLATFORM['viber']) {
                    $viberApi = new ViberApi($users[$j]);
                    $viberApi->sendMessage($text);
                }
                $res[$mails[$i]->id][] = $users[$j];
            }

            $mails[$i]->status = Mail::STATUS['SENT'];
            $mails[$i]->date_send = Yii::$app->formatter->asDatetime(time(), "php:Y-m-d H:i:s");
            $mails[$i]->save(false);
        }

        if ($id) {
            return $this->redirect(['index']);
        } else {
            return json_encode($res);
            // return '<pre>' . print_r($res, true) . '</pre>';
        }
    }
}
