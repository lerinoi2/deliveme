<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\modules\user\models\User;
use app\modules\bot\models\Bot;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="/web/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/web/favicon.ico" type="image/x-icon">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
  <!-- <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script> -->
  <!-- <script src="/js/libs/socket.io.js"></script> -->
  <script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
      OneSignal.init({
        appId: "<?=YII_ENV_DEV?'2ea9df06-2d05-4050-9a9e-b8b293bccc72':'7cf832d1-09c9-4f2b-b822-e0acc0f351cf'?>",
      });
    });
  </script>
</head>
<body class="def-bg" id="d">
  <?php $this->beginBody() ?>
  <?php if (!Yii::$app->user->isGuest):?>

  <script type="text/javascript">
    var global_userID = <?=Yii::$app->user->id?>;
    var global_userBotID = false;
    var global_reviewID = false;
  </script>
    
  <aside>
    <ul class="side-nav fixed">
      <li class="logo">
        <object type="image/png" data="/img/logo-text.png" class="logo-text">DeliveMe</object>
        <object type="image/png" data="/img/logo.png" class="logo-icon">DeliveMe</object>
      </li>

      <?
      $route = $this->context->route;
      $activeRoute = array(
        'dial' => false,
        'revi' => false,
        'mail' => false,
        'stat' => false,
        'user' => false
      );
      if(preg_match('/^dial(.*?)\/defa(.*?)/', $route)) {$activeRoute['dial'] = true;}
      if(preg_match('/^revi(.*?)\/defa(.*?)/', $route)) {$activeRoute['revi'] = true;}
      if(preg_match('/^mail(.*?)/', $route)) {$activeRoute['mail'] = true;}
      if(preg_match('/^stat(.*?)/', $route)) {$activeRoute['stat'] = true;}
      if(preg_match('/^users(.*?)/', $route)) {$activeRoute['user'] = true;}
      ?>

      <li class=" <?=$activeRoute['dial'] ? 'active' : ''?>">
        <a class="waves-effect waves-blue" href="/dialog">
          <i class="material-icons small left">forum</i>
          <div class="notifCount hide mes"></div>
          <span>Диалоги</span>
        </a>
      </li>
      <li class=" <?=$activeRoute['revi']?'active':''?>">
        <a class="waves-effect waves-blue" href="/review">
          <i class="material-icons small left">textsms</i>
          <div class="notifCount hide rev"></div>
          <span>Отзывы</span>
        </a>
      </li>
      <li class=" <?=$activeRoute['mail']?'active':''?>">
        <a class="waves-effect waves-blue" href="/mailing">
          <i class="material-icons small left">mail_outline</i>
          <span>Рассылки</span>
        </a>
      </li>
      <li class=" <?=$activeRoute['user']?'active':''?>">
        <a class="waves-effect waves-blue" href="/users">
          <i class="material-icons small left">people</i>
          <span>Пользователи</span>
        </a>
      </li>
      <li class=" <?=$activeRoute['stat']?'active':''?>">
        <a class="waves-effect waves-blue" href="/statistics">
          <i class="material-icons small left">trending_up</i>
          <span>Статистика</span>
        </a>
      </li>
      <!-- <li>
        <a class="waves-effect waves-blue" href="">
          <i class="material-icons small left">help_outline</i>
          <span>Помощь</span>
        </a>
      </li> -->
      <div class="toggle-item"><i class="material-icons small">chevron_left</i><span>Свернуть</span></div>
    </ul>
  </aside>
  <?if(isset($this->params['tabs']) && count($this->params['tabs']) > 0):?>
    <nav class="nav-extended">
      <div class="container-fluid nav-content">
        <ul class="tabs tabs-transparent">
          <?foreach($this->params['tabs'] as $key => $tab):?>
            <li class="tab"><a href="#<?=$key?>"><?=$tab?></a></li>
          <?endforeach;?>
        </ul>
      </div>
    </nav>
  <?endif;?>
    <!-- <nav class="breadcrumbs" style="<?=(isset($this->params['breadcrumbs']) && count($this->params['breadcrumbs']) > 0)? '':'display:none;'?>">
      <div class="container-fluid">
        <div class="nav-wrapper">
          <?= Breadcrumbs::widget([
            'tag'=>'div',
            'options'=>['class'=>'col s12'],
            'homeLink'=>false,
            'itemTemplate' => "{link}",
            'activeItemTemplate' => "<a class='breadcrumb'>{link}</a>",
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          ]);?>
        </div>
      </div>
    </nav> -->
  <main class="main">
    <nav class="def-bg">
      <div class="nav-wrapper">
        <div class="flex">
          <h1 class="title"><?= Html::encode($this->title) ?></h1>
          <div class="row">
            <!-- <div class="col s6"> -->
              <select class="top-select" disabled>
                <option value="" disabled>Выбрать бота</option>
                <?
                  $bots = Bot::find()->where(['user_id' => Yii::$app->user->id])->all();
                  foreach ($bots as $bot) {
                    echo '<option value="'.$bot->id.' '.($bot->id==1?'selected':'').'">'.$bot->name.'</option>';
                  }
                ?>
              </select>
            <!-- </div> -->
            <!-- <div class="col s6">
              <a class="waves-effect waves-light btn w230 disabled">Добавить бота <i class="material-icons left lhi">add</i></a>
            </div> -->
          </div>
          <ul class="right">
            <li><a class="waves-effect waves-light btn disabled"><i class="material-icons">help</i></a></li>
            <li><a class="waves-effect waves-light btn disabled">Профиль <i class="material-icons left">person</i></a></li>
            <li><a href="/logout" class="waves-effect waves-light btn">Выйти</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="content-container">
      <div class="content row m0">
        <?= $content ?>
      </div>
    </div>
  </main>

  <div id="modalConnect" class="modal">
    <div class="modal-content center-align">
      <h4>Пользователь хочет связаться с оператором!</h4>
      <a href="/dialog" class="btn modal-close waves-effect waves-blue">Перейти к диалогу</a>
    </div>
  </div>
  <audio id="notice">
    <source src="/audio/notice.ogg" type="audio/ogg">
    <source src="/audio/notice.mp3" type="audio/mpeg">
    <!-- <source src="notify.wav" type="audio/wav"> -->
  </audio>
  <?php else:?>
  <div class="container-signup">
    <div class="logo center-align">
      <object type="image/png" data="/img/logo-white.png">DeliveMe</object>
    </div>
    <div class="card">
      <?= $content ?>
    </div>
  </div>
  <?php endif;?>

  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
