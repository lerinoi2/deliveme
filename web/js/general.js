$(document).ready(function() {
    $('.modal').modal();
    $('select').material_select();
    $('.tooltipped').tooltip({
        html: true,
    });

    $('.toggle-item').click(function(){
        $('body').toggleClass('side-open');
    });

    $('.reviewForm select').on('change', function () {
        $('.reviewForm').submit();
    });

    $('.reviewForm input').keydown(function (e) {
        if(e.keyCode == 13) {
            $('.reviewForm').submit();
        }
    });

    $('.wrapper.messages').scrollTop(999999);

    $(document).on('click', '.link', function(){
        window.location.href = '/' + $(this).attr('data-section') + '/' + $(this).attr('data-id') + window.location.search;

        // Сохранение позиции скролла при переходе между страницами
        sessionStorage.setItem($(this).attr('data-section')+"Scroll", $(".wrapper.users").scrollTop());
    });

    $('.reset').click(function(){
        window.location.href = window.location.origin + window.location.pathname;
    });

    // $('.selectTemplate').click(function(){
    $('.templateId select').on('change', function(){
        var tempId = $('.templateId select').val();
        var pathBlock = $('.templateFiles .file-path');
        if (tempId) {
            $.post( "/mailing/templates/view/" + tempId, function(data) {
                data = JSON.parse(data);
                $('.templateText textarea').val(data.text);
                $('.templateText textarea').attr('disabled', true);
                $('.templateText label').addClass('active');
                var str = '';
                for (var i = 0; i < data.files.length; i++) {
                    str +=  data.files[i].n + (i==data.files.length-1?'':', ');
                }
                if (str) {
                    pathBlock.val(str);
                    pathBlock.attr('disabled', true);
                    $('.templateFiles [type="file"]').attr('disabled', true);
                }
                $('[name="temp_name"]').attr('disabled', true);
                $('[name="save_as_temp"]').attr('disabled', true);
            });
        } else {
            $('.templateText textarea').val('');
            $('.templateText textarea').removeAttr('disabled');
            $('.templateText label').removeClass('active');
            pathBlock.val('');
            pathBlock.removeAttr('disabled');
            $('.templateFiles [type="file"]').removeAttr('disabled');
            $('[name="temp_name"]').removeAttr('disabled');
            $('[name="save_as_temp"]').removeAttr('disabled');
        }
    });

    $('.booksForm input,.booksForm select').on('change', function () {
        $.post( "/mailing/books/users", $('.booksForm').serialize(), function(data) {
            $('.usersInput').val(data);

            // console.log(data);
            data = JSON.parse(data);

            $('.booksForm .counter').text(data.length);
        });
    });

    $('.sendBtn').on('click', function () {
        $(this).closest('form').submit();
    });

    $('.ajaxForm').on('submit', function (e) {
        e.preventDefault();
        var form = $(this),
            btn = form.find('.sendBtn');
        btn.addClass('disabled');
        $.ajax({
          url: form.attr('action'),
          data: form.serialize(),
          method: form.attr('method'),
          complete: function () {
            form.trigger( 'reset' );
            btn.removeClass('disabled');
          }
        });
    });

    $('.statsFilter input').on('change', function () {
        $('.statsFilter').submit();
    });

    $('.setFilterDate').on('click', function () {
        setFilterDate($(this).attr('data-value'));
        $('.statsFilter').submit();
    });

    $('.userConnect').on('click', function () {
        $(this).addClass('disabled');
        $.ajax($(this).attr('href')).done(function () {
          $('.userConnectBlock').toggleClass('connected');
          $('.userConnect.disabled').removeClass('disabled');
        });
    });

    $('.mail-form form').on('beforeSubmit', function () {
      $('.mail-form .preloader-container').addClass('show');
    });
    
    setTimepickers();
    initCharts();
    
    var path = (window.location.hostname.indexOf('.cktest.ru') !== -1)?'wss://ws.deliveme.cktest.ru':'wss://ws.deliveme.ga';
    socket(path);
    setNotifCounts();
});

function setNotifCounts() {
  $.ajax({
    url: '/bot/getuserinfo',
    async: false,
    success: function(text) {
      var data = JSON.parse(text);
      if (Number(data.messages) > 0) {
        $('.notifCount.mes').text(data.messages).removeClass('hide');
      } else {
        $('.notifCount.mes').addClass('hide');
      }
      if (Number(data.reviews) > 0) {
        $('.notifCount.rev').text(data.reviews).removeClass('hide');
      } else {
        $('.notifCount.rev').addClass('hide');
      }
    },
  })
}

function socket(path) {
  var roomID = global_userID,
      userBotID = global_userBotID,
      reviewID = global_reviewID,
      section = false,
      socket = io(path);

  if (window.location.pathname.indexOf('dialog') !== -1) {
    section = 'dialog';
  } else if (window.location.pathname.indexOf('review') !== -1) {
    section = 'review';
  }

  socket.on('connect', function () {
      socket.emit('subscribe', roomID);
  });

  socket.on('chat message', function (data) {
    // console.log(data);
    if (data.is_connect) {
      $('#modalConnect')
        .modal('open')
        .find('a').attr('href', '/dialog/' + data.user_id);
      $('#notice')[0].play();
      newPush('Пользователь подключился!', 'Чтобы перейти к диалогу с пользователем, нажмите на рассылку.', '/dialog/' + data.user_id);
    }

    if (data.user_id && !data.review_id && data.count <= 1) {
      Materialize.toast('Новый пользователь!', 2000);
    }

    if (data.connect == 0) {
      $('.userConnectBlock').removeClass('connected');
    }

    if (data.review_id && data.count <= 1) {
      Materialize.toast('Новый отзыв!', 2000);
      $('#notice')[0].play();
      newPush('Новый отзыв!', 'Чтобы перейти к отзыву, нажмите на рассылку.', '/review/' + data.review_id);
    }

    if (data.user_id == userBotID || data.review_id == reviewID) {
      $.ajax({
        url: '/bot/message/'+data.message_id+'?user_id='+data.user_id,
        async: false,
        success: function(text) {
          $('.messages').append(text);
          $('.messages').scrollTop(999999);
        },
      })
    }

    if (data.user_id && section == 'dialog') {
      $.ajax({
        url: '/dialog/user/'+data.user_id+'?user_id='+userBotID,
        async: false,
        success: function(text) {
          if (data.review_id) {
            $('.users__card[data-id="'+data.user_id+'"]').replaceWith(text);
          } else {
            if (data.count > 1) {
              $('.users__card[data-id="'+data.user_id+'"]').replaceWith(text);
            } else {
              $('.users').append(text);
            }
          }

          var $elements = $('.users > .users__card');
          $elements.sort(function (a, b) {
              if ($(b).attr('data-connect') == $(a).attr('data-connect')) {
                  return $(b).attr('data-date') - $(a).attr('data-date');
              }
              return ($(b).attr('data-connect') < $(a).attr('data-connect'))? -1 : 1;
          });
          $elements.appendTo($('.users'));
        },
      })
    }

    if (data.review_id && section == 'review') {
      $.ajax({
        url: '/review/user/'+data.review_id+'?review_id='+reviewID,
        async: false,
        success: function(text) {
          if (data.count > 1) {
            $('.users__card[data-id="'+data.review_id+'"]').replaceWith(text);
          } else {
            $('.users').append(text);
          }

          var $elements = $('.users > .users__card');
          $elements.sort(function (a, b) {
              if ($(a).attr('data-status') == $(b).attr('data-status')) {
                  return $(b).attr('data-date') - $(a).attr('data-date');
              }
              return $(a).attr('data-status') - $(b).attr('data-status');
          });
          $elements.appendTo($('.users'));
        },
      })
    }
    setNotifCounts();
  });

  function newPush(title, body, url = '') {
      OneSignal.sendSelfNotification(
          title,
          body,
          'http://' + window.location.hostname + url
      );
  }
}

function setTimepickers() {
    if($('.datetime-field input').length) {
        $('.datetime-field input').datetimepicker({
          format:'d.m.Y H:i',
          step:10
        });
        $.datetimepicker.setLocale('ru');
    }
    if($('.date-field input').length) {
        $('.date-field input').datetimepicker({
          format:'d.m.Y',
          timepicker: false
        });
        $.datetimepicker.setLocale('ru');
    }
    if($('.time-field input').length) {
        $('.time-field input').datetimepicker({
          format:'H:i',
          step:10,
          datepicker: false
        });
        $.datetimepicker.setLocale('ru');
    }
}

function initCharts() {
    if (!window.chart) {return;}
    Morris.Line({
      element: 'chart-users',
      data: chart.users,
      xkey: 'date',
      ykeys: ['count'],
      labels: ['Количество'],
      resize: true,
      smooth: false,
    });

    Morris.Line({
      element: 'chart-newusers',
      data: chart.newusers,
      xkey: 'date',
      ykeys: ['count'],
      labels: ['Количество'],
      resize: true,
      smooth: false,
    });

    Morris.Line({
      element: 'chart-getmes',
      data: chart.getmes,
      xkey: 'date',
      ykeys: ['count'],
      labels: ['Количество'],
      resize: true,
      smooth: false,
    });

    Morris.Line({
      element: 'chart-sendmes',
      data: chart.sendmes,
      xkey: 'date',
      ykeys: ['count'],
      labels: ['Количество'],
      resize: true,
      smooth: false,
    });

    Morris.Line({
      element: 'chart-reviews',
      data: chart.reviews,
      xkey: 'date',
      ykeys: ['count'],
      labels: ['Количество'],
      resize: true,
      smooth: false,
    });

    Morris.Line({
      element: 'chart-orders',
      data: chart.orders,
      xkey: 'date',
      ykeys: ['count'],
      labels: ['Количество'],
      resize: true,
      smooth: false,
    });
}

function setFilterDate(period) {
  var dateStart, dateEnd;
  switch(period) {
    case 'today':
      d = new Date();
      dateStart = formatDate(d);
      dateEnd = formatDate(new Date(d.getTime() + 86400 * 1000));
      break;
    case 'yesterday':
      d = new Date();
      dateStart = formatDate(new Date(d.getTime() - 86400 * 1000));
      dateEnd = formatDate(d);
      break;
    case 'week':
      d = new Date();
      dateStart = formatDate(new Date(d.setDate(d.getDate() - d.getDay() + 1)));
      dateEnd = formatDate(new Date(d.setDate(d.getDate() - d.getDay() + 8)));
      break;
    case 'month':
      d = new Date();
      dateStart = formatDate(new Date(d.getFullYear(),d.getMonth()));
      dateEnd = formatDate(new Date(d.getFullYear(),d.getMonth() + 1));
      break;
  }
  $('[name="date_start"]').val(dateStart);
  $('[name="date_end"]').val(dateEnd);
}

function formatDate(date) {
  var d = date.getDate();
  if (d < 10) d = '0' + d;

  var m = date.getMonth() + 1;
  if (m < 10) m = '0' + m;

  var y = date.getFullYear();
  if (y < 10) y = '0' + y;

  return d + '.' + m + '.' + y;
}

// Восстановление скролла
function updateScroll() {
  var section = false;
  if (window.location.pathname.indexOf('dialog') !== -1) {
    section = 'dialog';
  } else if (window.location.pathname.indexOf('review') !== -1) {
    section = 'review';
  }
  if (section) $(".wrapper.users").scrollTop(sessionStorage.getItem(section+"Scroll"));
}
updateScroll();