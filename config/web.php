<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Asia/Yekaterinburg',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@elephant' => '@vendor/wisembly/elephant.io'
    ],
    'modules' => [
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
        'dialog' => [
            'class' => 'app\modules\dialog\Module',
        ],
        'bot' => [
            'class' => 'app\modules\bot\Module',
        ],
        'review' => [
            'class' => 'app\modules\review\Module',
        ],
        'mailing' => [
            'class' => 'app\modules\mailing\Module',
        ],
        'webhooks' => [
            'class' => 'app\modules\webhooks\Module',
        ],
        'socket' => [
            'class' => 'app\modules\socket\Module',
        ],
        'statistics' => [
            'class' => 'app\modules\statistics\Module',
        ],
        'users' => [
            'class' => 'app\modules\users\Module',
        ],
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'RocYIHMxTQgp2Nt6X8esDSvroNnqAaY7',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'user/default/index',
                '/<_a:(login|logout|reset|password-reset|reguser)>' => 'user/signup/<_a>',
                //DIALOGS
                [
                    'pattern' => '/dialog/<id:[\d]+>',
                    'route' => 'dialog/default/index',
                    'defaults' => ['id' => ''],
                ],
                '/dialog/mute/<id:[\d]+>' => 'dialog/default/mute',
                '/dialog/user/<id:[\d]+>' => 'dialog/default/user',
                //REVIEWS
                [
                    'pattern' => '/review/<id:[\d]+>',
                    'route' => 'review/default/index',
                    'defaults' => ['id' => ''],
                ],
                '/dialog/checkconn/' => 'dialog/default/checkconn',
                '/review/complete/<id:[\d]+>' => 'review/default/complete',
                '/review/user/<id:[\d]+>' => 'review/default/user',
                //TELEGRAM API
                '/api/telegram/save_message' => 'webhooks/telegram/save',
                '/testsave' => 'webhooks/telegram/testsave', // TEST!!!
                '/api/telegram/send_message/<user_id:[\d]+>' => 'webhooks/telegram/send',
                '/api/telegram/update_photo/<user_id:[\d]+>' => 'webhooks/telegram/update',
                '/api/telegram/disable/<user_id:[\d]+>'  => 'webhooks/telegram/disable',
                '/api/telegram/enable/<user_id:[\d]+>' => 'webhooks/telegram/enable',
                '/api/telegram/test/' => 'webhooks/telegram/test', // TEST!!!
                //VIBER API
                '/api/viber/save_message' => 'webhooks/viber/save',
                '/api/viber/send_message/<user_id:[\d]+>' => 'webhooks/viber/send',
                '/api/viber/disable/<user_id:[\d]+>' => 'webhooks/viber/disable',
                '/api/viber/enable/<user_id:[\d]+>'  => 'webhooks/viber/enable',
                '/api/viber/update_photo/<user_id:[\d]+>' => 'webhooks/viber/update',
                //VK API
                '/api/vk/save_message' => 'webhooks/vk/save',
                '/api/vk/send_message/<user_id:[\d]+>' => 'webhooks/vk/send',
                '/api/vk/disable/<user_id:[\d]+>' => 'webhooks/vk/disable',
                '/api/vk/enable/<user_id:[\d]+>' => 'webhooks/vk/enable',
                '/api/vk/user/<user_id:[\d]+>' => 'webhooks/vk/user',
                //WEB SOCKET
                '/socket' => 'socket/default/start',
                '/socket/run' => 'socket/default/run-socket',
                //MAILING
                '/mailing' => 'mailing/default/index',
                '/mailing/create' => 'mailing/default/create',
                [
                    'pattern' => '/mailing/send/<id:[\d]+>',
                    'route' => 'mailing/default/send',
                    'defaults' => ['id' => ''],
                ],

                '/mailing/books' => 'mailing/book/index',
                '/mailing/books/create' => 'mailing/book/create',
                '/mailing/books/users' => 'mailing/book/users',

                '/mailing/templates' => 'mailing/template/index',
                '/mailing/templates/create' => 'mailing/template/create',
                '/mailing/templates/view/<id:[\d]+>' => 'mailing/template/view',
                //STATISTICS
                '/statistics' => 'statistics/default/index',
                //USERS
                '/users' => 'users/default/index',
                //BOT DEFAULT
                '/bot/message/<id:[\d]+>' => 'bot/default/message',
                '/bot/getuserinfo' => 'bot/default/getuserinfo',
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.88.102', '192.168.88.1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.88.102', '192.168.88.1'],
    ];
}

return $config;
