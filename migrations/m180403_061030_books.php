<?php

use yii\db\Migration;

/**
 * Class m180403_061030_books
 */
class m180403_061030_books extends Migration
{
    public function up()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'date'=> $this->dateTime(),
            'users'=> $this->text(),
            'platforms'=> $this->string(),
            'period'=> $this->string(),
            'orders'=> $this->string(),
            'reviews'=> $this->string(),
            'menu'=> $this->string(),
            'count'=> $this->integer(),
            'bot_id'=> $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-books-bot_id',
            '{{%books}}',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-books-bot_id',
            '{{%books}}',
            'bot_id',
            'bots',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-books-bot_id',
            '{{%books}}'
        );

        $this->dropIndex(
            'idx-books-bot_id',
            '{{%books}}'
        );
        
        $this->dropTable('{{%books}}');
    }
}
