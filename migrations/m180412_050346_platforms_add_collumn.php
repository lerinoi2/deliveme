<?php

use yii\db\Migration;

/**
 * Class m180412_050346_platforms_add_collumn
 */
class m180412_050346_platforms_add_collumn extends Migration
{
    public function up()
    {
        $this->addColumn('platforms', 'bot_url', $this->string());
    }

    public function down()
    {
        echo "m180412_050346_platforms_add_collumn cannot be reverted.\n";

        return false;
    }
}
