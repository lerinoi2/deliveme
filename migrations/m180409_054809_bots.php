<?php

use yii\db\Migration;

/**
 * Class m180409_054809_bots
 */
class m180409_054809_bots extends Migration
{
    public function up()
    {
        $this->createTable('{{%bots}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'description'=> $this->text(),
            'user_id'=> $this->integer()->notNull(),
        ]);
        
        $this->createIndex(
            'idx-bots-user_id',
            '{{%bots}}',
            'user_id'
        );

        $this->addForeignKey(
            'fk-bots-user_id',
            '{{%bots}}',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-bots-user_id',
            '{{%bots}}'
        );

        $this->dropIndex(
            'idx-bots-user_id',
            '{{%bots}}'
        );
        
        $this->dropTable('{{%bots}}');
    }
}
