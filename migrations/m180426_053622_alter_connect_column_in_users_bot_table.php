<?php

use yii\db\Migration;

/**
 * Class m180426_053622_alter_connect_column_in_users_bot_table
 */
class m180426_053622_alter_connect_column_in_users_bot_table extends Migration
{
    public function up(){
        $this->alterColumn('{{%users_bot}}', 'connect', $this->integer()->defaultValue(0));
    }

    public function down() {
        $this->alterColumn('{{%users_bot}}','connect', $this->boolean()->defaultValue(false) );
    }
}
