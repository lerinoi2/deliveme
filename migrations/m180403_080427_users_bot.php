<?php

use yii\db\Migration;

/**
 * Class m180403_080427_users_bot
 */
class m180403_080427_users_bot extends Migration
{
    public function up()
    {
        $this->createTable('{{%users_bot}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image'=> $this->string(),
            'platform' => $this->integer()->notNull(),
            'chat_id'=> $this->string()->notNull(),
            'bot_id' => $this->integer()->notNull(),
        ]);
        
        $this->createIndex(
            'idx-users_bot-bot_id',
            '{{%users_bot}}',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-users_bot-bot_id',
            '{{%users_bot}}',
            'bot_id',
            'bots',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-users_bot-bot_id',
            '{{%users_bot}}'
        );

        $this->dropIndex(
            'idx-users_bot-bot_id',
            '{{%users_bot}}'
        );

        $this->dropTable('{{%users_bot}}');
    }
}
