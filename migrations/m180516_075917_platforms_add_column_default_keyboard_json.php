<?php

use yii\db\Migration;

/**
 * Class m180516_075917_platforms_add_column_default_keyboard_json
 */
class m180516_075917_platforms_add_column_default_keyboard_json extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%platforms}}', 'default_keyboard_json', $this->string(1000));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%platforms}}', 'default_keyboard_json');
    }
}
