<?php

use yii\db\Migration;

/**
 * Handles adding is_read to table `messages`.
 */
class m180416_120904_add_is_read_column_to_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%messages}}', 'is_read', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%messages}}', 'is_read');
    }
}
