<?php

use yii\db\Migration;

/**
 * Class m180425_044944_add_orders_to_users_bot_table
 */
class m180425_044944_add_orders_to_users_bot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%users_bot}}', 'orders', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%users_bot}}', 'orders');
    }
}
