<?php

use yii\db\Migration;

/**
 * Class m180409_054201_platforms
 */
class m180409_054201_platforms extends Migration
{
    public function up()
    {
        $this->createTable('{{%platforms}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description'=> $this->text(),
            'token'=> $this->string(),
            'token_botan'=> $this->string(),
            'bot_id' => $this->integer()->notNull(),
            'platform_id' => $this->integer()->notNull(),
        ]);
        
        $this->createIndex(
            'idx-platforms-bot_id',
            '{{%platforms}}',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-platforms-bot_id',
            '{{%platforms}}',
            'bot_id',
            'bots',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-platforms-bot_id',
            '{{%platforms}}'
        );

        $this->dropIndex(
            'idx-platforms-bot_id',
            '{{%platforms}}'
        );

        $this->dropTable('{{%platforms}}');
    }
}
