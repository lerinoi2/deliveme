<?php

use yii\db\Migration;

/**
 * Handles the creation of table `buttons`.
 */
class m180410_064443_create_buttons_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%telegram_buttons}}', [
            'id' => $this->primaryKey(),
            'callback_json'=> $this->string(),
            'text'=> $this->string()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%telegram_buttons}}');
    }
    /**
    public function safeUp()
    {
        $this->createTable('buttons', [
            'id' => $this->primaryKey(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('buttons');
    }
    */
}
