<?php

use yii\db\Migration;

/**
 * Handles adding postpone_date to table `mails`.
 */
class m180413_053718_add_postpone_date_column_to_mails_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%mails}}', 'postpone_date', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%mails}}', 'postpone_date');
    }
}
