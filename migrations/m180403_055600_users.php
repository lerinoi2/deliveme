<?php

use yii\db\Migration;

/**
 * Class m180403_055600_users
 */
class m180403_055600_users extends Migration
{
    public function up()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->unique(),
            'password'=> $this->string()->notNull(),
            'role'=> $this->integer()->notNull(),
            'phone'=> $this->string(),
            'email'=> $this->string(),
            'confirm'=> $this->boolean()->defaultValue(false)
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%users}}');
    }
}
