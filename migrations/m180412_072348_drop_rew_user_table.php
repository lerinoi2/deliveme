<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `rew_user`.
 */
class m180412_072348_drop_rew_user_table extends Migration
{
    public function up()
    {
        $this->dropTable('reviews_users');
    }
}
