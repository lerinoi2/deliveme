<?php

use yii\db\Migration;

/**
 * Class m180411_060824_add_review_collumn
 */
class m180411_060824_add_review_collumn extends Migration
{

    public function up()
    {
        $this->addColumn('reviews', 'complete', $this->boolean());
    }

    public function down()
    {
        echo "m180411_060824_add_review_collumn cannot be reverted.\n";

        return false;
    }
}
