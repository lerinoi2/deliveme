<?php

use yii\db\Migration;

/**
 * Handles adding date to table `users_bot`.
 */
class m180418_114339_add_date_column_to_users_bot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%users_bot}}', 'date', $this->integer()->defaultValue(time()));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%users_bot}}', 'date');
    }
}
