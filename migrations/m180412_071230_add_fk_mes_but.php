<?php

use yii\db\Migration;

/**
 * Class m180412_071230_add_fk_mes_but
 */
class m180412_071230_add_fk_mes_but extends Migration
{
    public function up()
    {
        $this->createIndex(
            'idx-telegram_message_buttons-message_id',
            '{{%telegram_message_buttons}}',
            'message_id'
        );

        $this->addForeignKey(
            'fk-telegram_message_buttons-message_id',
            '{{%telegram_message_buttons}}',
            'message_id',
            'messages',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-telegram_message_buttons-button_id',
            '{{%telegram_message_buttons}}',
            'button_id'
        );

        $this->addForeignKey(
            'fk-telegram_message_buttons-button_id',
            '{{%telegram_message_buttons}}',
            'button_id',
            'telegram_buttons',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-telegram_message_buttons-message_id',
            '{{%telegram_message_buttons}}'
        );

        $this->dropIndex(
            'idx-telegram_message_buttons-message_id',
            '{{%telegram_message_buttons}}'
        );
        
        $this->dropForeignKey(
            'fk-telegram_message_buttons-message_id',
            '{{%telegram_message_buttons}}'
        );

        $this->dropIndex(
            'idx-telegram_message_buttons-message_id',
            '{{%telegram_message_buttons}}'
        );
    }
}
