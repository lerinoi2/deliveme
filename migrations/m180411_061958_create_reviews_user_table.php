<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reviews_users`.
 */
class m180411_061958_create_reviews_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('reviews_users', [
            'id' => $this->primaryKey(),
            'user_id'=> $this->integer()->notNull(),
            'review_id'=> $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('reviews_users');
    }
}
