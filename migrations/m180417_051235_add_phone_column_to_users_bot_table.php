<?php

use yii\db\Migration;

/**
 * Handles adding phone to table `users_bot`.
 */
class m180417_051235_add_phone_column_to_users_bot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%users_bot}}', 'phone', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%users_bot}}', 'phone');
    }
}
