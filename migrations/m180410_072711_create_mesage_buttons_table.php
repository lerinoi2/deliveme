<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mesage_buttons`.
 */
class m180410_072711_create_mesage_buttons_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%telegram_message_buttons}}', [
            'id' => $this->primaryKey(),
            'message_id'=> $this->integer()->notNull(),
            'button_id'=> $this->integer()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%telegram_message_buttons}}');
    }
}
