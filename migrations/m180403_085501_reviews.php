<?php

use yii\db\Migration;

/**
 * Class m180403_085501_reviews
 */
class m180403_085501_reviews extends Migration
{
    public function up()
    {
        $this->createTable('{{%reviews}}', [
            'id' => $this->primaryKey(),
            'status'=> $this->integer()->notNull()->defaultValue(0),
            'order_id' => $this->integer(),
            'mark' => $this->integer(),
            'operator_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%reviews}}');
    }
}
