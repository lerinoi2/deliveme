<?php

use yii\db\Migration;

/**
 * Handles adding muted to table `users_bot`.
 */
class m180417_054948_add_muted_column_to_users_bot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%users_bot}}', 'muted', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%users_bot}}', 'muted');
    }
}
