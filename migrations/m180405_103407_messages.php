<?php

use yii\db\Migration;

/**
 * Class m180405_103407_messages
 */
class m180405_103407_messages extends Migration
{
    public function up()
    {
        $this->createTable('{{%messages}}', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'message_id' => $this->string(),
            'action'=> $this->string(),
            'date'=> $this->dateTime(),
            'is_bot'=> $this->boolean()->notNull()->defaultValue(false),
            'user_bot_id' => $this->integer(),
            'review_id' => $this->integer(),
        ]);
        
        $this->createIndex(
            'idx-messages-user_bot_id',
            '{{%messages}}',
            'user_bot_id'
        );

        $this->addForeignKey(
            'fk-messages-user_bot_id',
            '{{%messages}}',
            'user_bot_id',
            'users_bot',
            'id',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-messages-review_id',
            '{{%messages}}',
            'review_id'
        );

        $this->addForeignKey(
            'fk-messages-review_id',
            '{{%messages}}',
            'review_id',
            'reviews',
            'id',
            'SET NULL'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-messages-user_bot_id',
            '{{%messages}}'
        );

        $this->dropIndex(
            'idx-messages-user_bot_id',
            '{{%messages}}'
        );
        
        $this->dropForeignKey(
            'fk-messages-review_id',
            '{{%messages}}'
        );

        $this->dropIndex(
            'idx-messages-review_id',
            '{{%messages}}'
        );

        $this->dropTable('{{%messages}}');
    }
}
