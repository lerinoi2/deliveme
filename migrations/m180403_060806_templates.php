<?php

use yii\db\Migration;

/**
 * Class m180403_060806_templates
 */
class m180403_060806_templates extends Migration
{
    public function up()
    {
        $this->createTable('{{%templates}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'text'=> $this->text()->notNull(),
            'files'=> $this->text(),
            'date'=> $this->dateTime(),
            'bot_id'=> $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-templates-bot_id',
            '{{%templates}}',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-templates-bot_id',
            '{{%templates}}',
            'bot_id',
            'bots',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-templates-bot_id',
            '{{%templates}}'
        );
        
        $this->dropIndex(
            'idx-templates-bot_id',
            '{{%templates}}'
        );
        
        $this->dropTable('{{%templates}}');
    }
}
