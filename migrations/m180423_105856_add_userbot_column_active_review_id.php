<?php

use yii\db\Migration;

/**
 * Class m180423_105856_add_userbot_column_active_review_id
 */
class m180423_105856_add_userbot_column_active_review_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%users_bot}}', 'active_review_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%users_bot}}', 'active_review_id');
    }
}
