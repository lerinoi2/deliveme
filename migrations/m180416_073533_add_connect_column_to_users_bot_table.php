<?php

use yii\db\Migration;

/**
 * Handles adding connect to table `users_bot`.
 */
class m180416_073533_add_connect_column_to_users_bot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%users_bot}}', 'connect', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%users_bot}}', 'connect');
    }
}
