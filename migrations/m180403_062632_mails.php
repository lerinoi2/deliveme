<?php

use yii\db\Migration;

/**
 * Class m180403_062632_mails
 */
class m180403_062632_mails extends Migration
{
    public function up()
    {
        $this->createTable('{{%mails}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'date_create'=> $this->dateTime(),
            'status'=> $this->integer()->defaultValue(0),
            'date_send'=> $this->dateTime(),
            'template_id' => $this->integer(),
            'book_id' => $this->integer(),
            'plan'=> $this->string(),
            'text'=> $this->text(),
            'files'=> $this->string(),
            'bot_id'=> $this->integer()->notNull(),
        ]);
        
        $this->createIndex(
            'idx-mails-template_id',
            '{{%mails}}',
            'template_id'
        );

        $this->addForeignKey(
            'fk-mails-template_id',
            '{{%mails}}',
            'template_id',
            'templates',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-mails-book_id',
            '{{%mails}}',
            'book_id'
        );

        $this->addForeignKey(
            'fk-mails-book_id',
            '{{%mails}}',
            'book_id',
            'books',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-mails-bot_id',
            '{{%mails}}',
            'bot_id'
        );

        $this->addForeignKey(
            'fk-mails-bot_id',
            '{{%mails}}',
            'bot_id',
            'bots',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-mails-template_id',
            '{{%mails}}'
        );

        $this->dropIndex(
            'idx-mails-template_id',
            '{{%mails}}'
        );

        $this->dropForeignKey(
            'fk-mails-book_id',
            '{{%mails}}'
        );

        $this->dropIndex(
            'idx-mails-book_id',
            '{{%mails}}'
        );

        $this->dropForeignKey(
            'fk-mails-bot_id',
            '{{%mails}}'
        );

        $this->dropIndex(
            'idx-mails-bot_id',
            '{{%mails}}'
        );
        
        $this->dropTable('{{%mails}}');
    }
}
