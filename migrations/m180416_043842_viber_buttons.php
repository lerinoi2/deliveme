<?php

use yii\db\Migration;

/**
 * Class m180416_043842_viber_buttons
 */
class m180416_043842_viber_buttons extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('viber_buttons', [
            'id' => $this->primaryKey(),
            'columns' => $this->integer(),
            'rows' => $this->integer(),
            'bg_color' => $this->string(),
            'silent' => $this->boolean(),
            'bg_media_type' => $this->string(),
            'bg_media' => $this->string(),
            'bg_loop' => $this->boolean(),
            'action_type' => $this->string(),
            'action_body' => $this->string(),
            'image' => $this->string(),
            'text' => $this->string(),
            'text_v_align' => $this->string(),
            'text_h_align' => $this->string(),
            'text_paddings' => $this->string(),
            'text_opacity' => $this->integer(),
            'text_size' => $this->string(),
            'open_u_r_l_type' => $this->string(),
            'open_u_r_l_media_type' => $this->string(),
            'text_bg_gradient_color' => $this->string(),
            'message_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-button-message_id',
            'viber_buttons',
            'message_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'idx-button-message_id',
            'viber_buttons',
            'message_id',
            'messages',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'idx-button-message_id',
            'viber_buttons'
        );

        $this->dropIndex(
            'idx-post-author_id',
            'viber_buttons'
        );

        $this->dropTable('viber_buttons');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180416_043842_viber_buttons cannot be reverted.\n";

        return false;
    }
    */
}
