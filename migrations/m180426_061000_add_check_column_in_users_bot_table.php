<?php

use yii\db\Migration;

/**
 * Class m180426_061000_add_check_column_in_users_bot_table
 */
class m180426_061000_add_check_column_in_users_bot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%users_bot}}', 'check', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%users_bot}}', 'check');
    }
}
